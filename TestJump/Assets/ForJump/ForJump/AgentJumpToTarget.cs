﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class AgentJumpToTarget : MonoBehaviour
{   
    NavMeshAgent navAgent;
    Rigidbody rBody;

    public GameObject Target;

    public float maxJumpDistance = 3f;
    public float jumpSpeed = 1f;

    Vector3 jumpStartPoint;
    Vector3 jumpEndPoint;
    List<Vector3> jumpPath = new List<Vector3>();
    NavMeshPath navMeshPath;
    float jumpDistance;

    GameObject dummyAgent;
    bool previousRigidBodyState;

    bool needsToJump = false;
    bool jumping = false;

    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navMeshPath = new NavMeshPath();
        rBody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (navAgent.isOnNavMesh && navAgent.isStopped == false)
        {
            navMeshPath = new NavMeshPath();
            navAgent.CalculatePath(Target.transform.position, navMeshPath);
            if (!navAgent.pathPending)
            {
                navAgent.path = navMeshPath;
                if (navMeshPath.status == NavMeshPathStatus.PathPartial)
                {
                    needsToJump = true;
                    jumpStartPoint = navMeshPath.corners[navMeshPath.corners.Length - 1];
                }
            }
            else
            {
                navAgent.isStopped = true;
                navAgent.ResetPath();
            }
        }

        if (needsToJump && !jumping)
        {
            if ((transform.position - jumpStartPoint).magnitude <= 0.75f)
            {
                if (navAgent.isOnNavMesh)
                    navAgent.isStopped = true;
                Jump();
            }
        }
    }

    void Jump()
    {
        needsToJump = false;
        jumping = true;

        //Spawn agent
        dummyAgent = Instantiate(new GameObject(), Target.transform);
        dummyAgent.AddComponent<NavMeshAgent>();
        dummyAgent.AddComponent<ReturnNavmeshInfo>();

        //get end point
        jumpEndPoint = dummyAgent.GetComponent<ReturnNavmeshInfo>().ReturnClosestPointBackToAgent(transform.position);
        jumpEndPoint = new Vector3(jumpEndPoint.x, jumpEndPoint.y + (navAgent.height / 2), jumpEndPoint.z);

        //make path
        jumpPath.Clear();
        jumpPath.Add(transform.position);

        Vector3 midPoint = Vector3.Lerp(transform.position, jumpEndPoint, 0.5f);
        midPoint.y = midPoint.y + navAgent.height;

        jumpPath.Add(midPoint);
        jumpPath.Add(jumpEndPoint);

        jumpDistance = Vector3.Distance(jumpStartPoint, jumpEndPoint);

        //jump
        if (jumpDistance <= maxJumpDistance)
        {
            previousRigidBodyState = rBody.isKinematic;
            navAgent.enabled = false;
            rBody.isKinematic = true;

            rBody.DOLocalPath(jumpPath.ToArray(), ((jumpDistance * 3) / (jumpSpeed * 4)), PathType.CatmullRom).OnComplete(JumpFinished);
        }
    }

    void JumpFinished()
    {
        navAgent.enabled = true;
        rBody.isKinematic = previousRigidBodyState;

        jumping = false;

        Destroy(dummyAgent);
    }
}
