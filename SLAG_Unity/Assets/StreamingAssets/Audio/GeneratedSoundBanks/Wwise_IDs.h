/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID PLAY_DEFAULT_FOOTSTEPS = 1704217537U;
        static const AkUniqueID PLAY_JUMP_VOICE = 3003882221U;
        static const AkUniqueID PLAY_SWAMP_BGM = 3366725289U;
        static const AkUniqueID PLAY_SWAMP_FOOTSTEPS = 2494826898U;
        static const AkUniqueID STOP_DEFAULT_FOOTSTEPS = 2019928655U;
        static const AkUniqueID STOP_SWAMP_BGM = 1222134263U;
    } // namespace EVENTS

    namespace SWITCHES
    {
        namespace FOOTSTEPS
        {
            static const AkUniqueID GROUP = 2385628198U;

            namespace SWITCH
            {
                static const AkUniqueID DEFAULT = 782826392U;
                static const AkUniqueID SWAMP = 2907906111U;
            } // namespace SWITCH
        } // namespace FOOTSTEPS

    } // namespace SWITCHES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID CHARACTER_SOUNDS = 2712437113U;
        static const AkUniqueID SWAMP_BANK = 3859623424U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID BGM = 412724365U;
        static const AkUniqueID CHARACTER_SOUNDS = 2712437113U;
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
