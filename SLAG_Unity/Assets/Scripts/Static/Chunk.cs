﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk
{
    public GameObject gObject;
    public int x, y;
    public Vector2 pos;
    public bool active = false;
    public bool shouldBeActive = false;
    public List<Item> worldDetail = new List<Item>();
    public List<Item> objects = new List<Item>();

    public Chunk(GameObject gObjectT, int xT, int yT)
    {
        gObject = gObjectT;
        x = xT;
        y = yT;
        pos = new Vector2(x, y);
    }

    public static List<Vector2> directions = new List<Vector2>(){
        new Vector2(1, 0), new Vector2(0, -1),
        new Vector2(-1, 0), new Vector2(0, 1),
        new Vector2(1, 1), new Vector2(1, -1),
        new Vector2(-1, 1), new Vector2(-1, -1),
    };

    public static Vector2 CompareChunks(Chunk A, Chunk B)
    {
        return new Vector2(B.x - A.x, B.y - A.y);
    }
}