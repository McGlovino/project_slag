﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{


    //Time
    public static float cactusFlowerRegrow = 20.0f;
    public static float fruitRegrow = 30.0f;
    public static int mushroomGrowChance = 4000; // persec


    //Durability
    public static int flimsyDurability = 10;
    public static int woodDurability = 25;
    public static int stoneDurability = 40;
}
