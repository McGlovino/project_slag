﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Functions
{
    public static float RangeChange(float OldValue, float OldMin, float OldMax, float NewMin, float NewMax)
    {
        float NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin;
        return NewValue;

    }
    //Standard
    public static bool isGrounded(Vector3 v)
    {
        return (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }
    public static bool isGrounded(Transform t)
    {
        return (Physics.Raycast(t.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }
    public static bool isGrounded(GameObject g)
    {
        return (Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f));
    }

    //Length
    public static bool isGrounded(Vector3 v, float length)
    {
        return (Physics.Raycast(v + new Vector3(0, length-0.1f, 0), Vector3.down, length));
    }
    public static bool isGrounded(Transform t, float length)
    {
        return (Physics.Raycast(t.position + new Vector3(0, length - 0.1f, 0), Vector3.down, length));
    }
    public static bool isGrounded(GameObject g, float length)
    {
        return (Physics.Raycast(g.transform.position + new Vector3(0, length - 0.1f, 0), Vector3.down, length));
    }

    //LayerMask
    public static bool isGrounded(Vector3 v, LayerMask m)
    {
        return (Physics.Raycast(v + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f, m));
    }
    public static bool isGrounded(Transform t, LayerMask m)
    {
        return (Physics.Raycast(t.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f, m));
    }
    public static bool isGrounded(GameObject g, LayerMask m)
    {
        return (Physics.Raycast(g.transform.position + new Vector3(0, 0.1f, 0), Vector3.down, 0.1f, m));
    }
}
