﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public static class RecipeList
{
    public static Recipe[] recipes = {
        //Tools
        new Recipe(new ITM_Torch(false),
            new Item[]{new ITM_Stick(), new ITM_Grass()},
            new int[]{2, 3}),

        new Recipe(new ITM_AxeStone(false),
            new Item[]{new ITM_Stick(), new ITM_Stone()},
            new int[]{2, 3}),

        new Recipe(new ITM_PickStone(false),
            new Item[]{new ITM_Stick(), new ITM_Stone()},
            new int[]{2, 3}),

        new Recipe(new ITM_AxeWood(false),
            new Item[]{new ITM_Stick(), new ITM_Wood()},
            new int[]{2, 3}),

        new Recipe(new ITM_PickWood(false),
            new Item[]{new ITM_Stick(), new ITM_Wood()},
            new int[]{2, 3}),

        new Recipe(new ITM_AxeFlimsy(false),
            new Item[]{new ITM_Stick()},
            new int[]{5}),

        new Recipe(new ITM_PickFlimsy(false),
            new Item[]{new ITM_Stick()},
            new int[]{5}),

        //Other
        new Recipe(new ITM_Rock(), 
            new Item[]{new ITM_Stone() }, 
            new int[]{8}),

        new Recipe(new ITM_BigRock(),
            new Item[]{new ITM_Rock() },
            new int[]{3})
    };
}
