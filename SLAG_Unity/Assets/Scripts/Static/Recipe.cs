﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recipe
{
    public Item[] ingredients;
    public int[] costs;
    public Item crafting;

    public Recipe(Item craftingT, Item[] ingredientsT, int[] costsT)
    {
        crafting = craftingT;
        ingredients = ingredientsT;
        costs = costsT;
    }

    public Recipe()
    {

    }
}
