﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class Inventory
{
    public List<List<Item>> tools;

    public List<List<Item>> items;

    public List<Recipe> recipes;
    public List<int> recipiesCanCraft;

    int maxCapacity = 9999999;

    public Inventory()
    {
        items = new List<List<Item>>();
        tools = new List<List<Item>>();
        recipes = new List<Recipe>();
        recipiesCanCraft = new List<int>();
    }

    public bool InInventory(Item item, List<List<Item>> list)
    {
        foreach(List<Item> i in list)
        {
            if (i[0].oName == item.oName)
                return true;
        }
        return false;
    }

    public bool AddItem(Item single, List<List<Item>> addTo = null)
    {
        if (addTo == null)
            addTo = items;
        if (single != null)
        {
            bool added = false;
            foreach (List<Item> i in addTo)
            {
                if (i[0].oName == single.oName && i.Count < maxCapacity)
                {
                    i.Add(single);
                    added = true;
                }
            }
            if (!added)
            {
                addTo.Add(new List<Item>());
                addTo[addTo.Count - 1].Add(single);
                return true; // True for adding new
            }
        }
        return false;
    }

    public void AddItem(List<Item> multiple)
    {

    }

    public Item RemoveItem(int position, List<List<Item>> list)
    {
        if (items.Count > 0)
        {
            Item temp = list[position][0];
            list[position].Remove(list[position][0]);
            if (list[position].Count < 1)
                list.Remove(list[position]);
            return temp;
        }
        return null;
    }

    public void RemoveItem(Item toRemove, List<List<Item>> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            for (int j = 0; j < list[i].Count; j++)
            {
                if (list[i][j] == toRemove)
                {
                    list[i].Remove(list[i][j]);
                    if (list[i].Count < 1)
                        list.Remove(list[i]);
                    return;
                }
            }
        }
    }

    public string PrintContents()
    {
        if (items.Count < 1)
            return "No Items";

        string toPrint = "";
        foreach (List<Item> i in items)
            toPrint += i[0].oName + ": " + i.Count + "\n";
        return toPrint;
    }

    public List<string> ListOfContents(List<List<Item>> list)
    {
        List<string> toPrint = new List<string>();

        if (list.Count < 1) {
            if (list != items)
                return null;
            else
            {
                toPrint.Add("No Items");
                return toPrint;
            }
        }

        foreach (List<Item> i in list)
            toPrint.Add(i.Count + (i.Count < 10 ? "\t\t" : "\t") + "|\t" + i[0].oName); //(i.Count < 100 ? (i.Count < 10 ? "  " : " " ) : "")
        return toPrint;
    }

    public string Content(int position)
    {
        if (items.Count < 1)
            return "No Items";
        return  items[position][0].oName + " " + items[position].Count;
    }

    public List<string> CalculateRecipies()
    {
        recipes.Clear();
        recipiesCanCraft.Clear();

        foreach (Recipe r in RecipeList.recipes)
        {
            //Add the amounts of each item in the recipe
            int[] amounts = new int[r.ingredients.Length];
            for (int j = 0; j < r.ingredients.Length; j++)
            {
                foreach (List<Item> i in items)
                {
                    if (i[0].oName == r.ingredients[j].oName && i.Count >= r.costs[j])
                    {
                        amounts[j] = Mathf.FloorToInt(i.Count / r.costs[j]);
                        break;
                    }
                }
            }
            //Add the amount that can be crafted
            if(Mathf.Min(amounts) > 0)
            {
                recipes.Add(r);
                recipiesCanCraft.Add(Mathf.Min(amounts));
            }
        }

        List<string> rv = new List<string>();
        for (int i = 0; i < recipes.Count; i++)
            rv.Add(recipiesCanCraft[i] + (recipiesCanCraft[i] < 10 ? "\t\t" : "\t") + "|\t" + recipes[i].crafting.oName);

        return (rv.Count > 0 ? rv : null);
    }

    public void CraftItem(int location)
    {
        Recipe focus = recipes[location - tools.Count - items.Count];
        for (int i = 0; i < focus.ingredients.Length; i++)
        {
            foreach(List<Item> j in items)
            {
                if(j[0].oName == focus.ingredients[i].oName)
                {
                    j.RemoveRange(j.Count - focus.costs[i], focus.costs[i]);
                    if (j.Count < 1)
                        items.Remove(j);
                    break;
                }
            }
        }

        Type type = focus.crafting.GetType();
        ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);
        if(focus.crafting.type == Item.typeOf.tool)
            AddItem((Item)constructor.Invoke(null), tools);
        else
            AddItem((Item)constructor.Invoke(null));
    }
}
