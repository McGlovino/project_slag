﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grow : MonoBehaviour
{
    public GameObject toGrowInto;
    public float timeToGrow; // seconds
    float counter;
    public bool chance = false;
    int roll;
    public Item thisItem;

    void Start()
    {
        if (chance)
            timeToGrow = 1; // becomes second counter
    }

    void Update()
    {
        if (thisItem != null)
        {
            if (!chance)
            {
                if (counter >= timeToGrow)
                {
                    thisItem.gObject = Instantiate(toGrowInto, transform.position, transform.rotation);
                    thisItem.oName = "Big Mushroom";
                    Destroy(this.gameObject);
                }
                counter += Time.deltaTime;
            }
            else
            {
                if (counter >= timeToGrow)
                {
                    timeToGrow++;
                    switch (thisItem.oName)
                    {
                        case "Mushroom":
                            roll = Random.Range(0, Constants.mushroomGrowChance);
                            if (roll == 0)
                            {
                                thisItem.gObject = Instantiate(toGrowInto, transform.position, transform.rotation, transform.parent);
                                thisItem.oName = "Big Mushroom";
                                Destroy(this.gameObject);
                            }
                            break;
                        default:
                            break;
                    }
                }
                counter += Time.deltaTime;
            }
        }
    }
}
