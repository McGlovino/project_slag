﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect : MonoBehaviour
{
    public bool hasCollected = false;
    List<GameObject> toCollect = new List<GameObject>();
    GameObject im;
    float counter;

    void Start()
    {
        im = GameObject.FindGameObjectWithTag("im");
        foreach (Transform child in this.transform.GetChild(0))
        {
            if (child.tag == "Drop")
                toCollect.Add(child.gameObject);
        }
    }

    public void CollectObj()
    {
        if (!hasCollected)
        {
            foreach (GameObject g in toCollect)
            {
                g.SetActive(false);
            }
            hasCollected = true;
        }
    }

    void Update()
    {
        if (counter >= Constants.cactusFlowerRegrow)
        {
            foreach (GameObject g in toCollect)
                g.SetActive(true);

            GetComponent<Animation>().Play("Return Fruit");
            hasCollected = false;
            counter = 0;
        }
        if(hasCollected)
            counter+=Time.deltaTime;
    }
}
