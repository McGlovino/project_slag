﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Item
{
    public enum typeOf { pickup, collect, select, tool, nothing };
    public enum toolTypeOf { PickAxe, Axe, Spade, Torch, nothing };

    public string oName;
    public GameObject gObject;
    public typeOf type;
    public toolTypeOf toolType = toolTypeOf.nothing;
    public Vector3 spawnPos = new Vector3();
    public Chunk chunk;

    public bool canPlace = true;


    public float durability, maxDurability;

    public Item()
    {
    }

    public Item(string oNameT, typeOf typeT)
    {
        oName = oNameT;
        type = typeT;
    }

    public virtual Item OtherCollect(Item selected = null)
    {
        if (!gObject.GetComponent<Collect>().hasCollected)
            return new Item();
        else
            return null;
    }

    public void Place(Transform player, Chunk chunkT)
    {
        spawnPos = player.position + player.GetChild(0).forward * 1.1f;
        spawnPos = new Vector3(spawnPos.x, 0, spawnPos.z);
        chunk = chunkT;
        chunk.objects.Add(this);
        if (gObject == null)
        {
            if (type == typeOf.tool)
                gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/" + oName));
            else
                gObject = ObjectPooler.Instance.GetFromQueue(oName, spawnPos, Quaternion.identity, chunk.gObject.transform);
        }
        else
            gObject.transform.position = spawnPos;
        gObject.transform.SetParent(chunk.gObject.transform, true);
        //gObject.transform.parent = chunk.gObject.transform;
        //Debug.Log("CHUNK: " + chunk.objects.IndexOf(this));
    }

    public Item PickUp(Item selected)
    {
        switch(type)
        {
            case (Item.typeOf.collect):
                return OtherCollect(selected);
            case (Item.typeOf.pickup):
                if (gObject != null)
                {
                    ClickControl.changeTo(gObject.transform, "Default");
                    chunk.objects.Remove(this);
                    ObjectPooler.Instance.poolDictionary[oName].Enqueue(gObject);
                    gObject.transform.position = new Vector3(10000000, 10000000, 10000000);
                    gObject = null;
                    return this;
                }
                else 
                    return null;
            case (Item.typeOf.tool):
                ClickControl.changeTo(gObject.transform, "Default");
                chunk.objects.Remove(this);
                Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
                gObject.transform.position = toolPos.position;
                gObject.transform.rotation = toolPos.transform.rotation;
                gObject.transform.SetParent(toolPos, true);
                chunk = null;
                //gObject.transform.parent = toolPos;
                break;
            default:
                ClickControl.changeTo(gObject.transform, "Default");
                chunk.objects.Remove(this);
                gObject.transform.position = new Vector3(10000000, 10000000, 10000000);
                //GameObject.Destroy(gObject);
                return this;
        }
        return this;
    }

    public IEnumerator Fall(float forwardBy = 0f, float fallSpeed = 3.5f)
    {
        Vector3 fallTo = new Vector3();

        gObject.transform.LookAt(new Vector3(Camera.main.transform.position.x, gObject.transform.position.y, Camera.main.transform.position.z));
        fallTo = new Vector3(gObject.transform.position.x, 0, gObject.transform.position.z);
        if (forwardBy > 0)
            fallTo += gObject.transform.GetChild(0).forward * forwardBy;
                
        while (gObject.transform.position.y > 0)
        {
            yield return new WaitForEndOfFrame();
            //gObject.transform.position = Vector3.Lerp(gObject.transform.position, fallTo, fallSpeed * Time.deltaTime);

            Vector3 dir = fallTo - gObject.transform.position;
            gObject.transform.position += dir.normalized * ((fallSpeed * Time.deltaTime) < dir.magnitude ? (fallSpeed * Time.deltaTime) : dir.magnitude);
        }
        gObject.transform.position = new Vector3(gObject.transform.position.x, 0, gObject.transform.position.z);
        spawnPos = gObject.transform.position;
    }

    public virtual void Damage(float damage = 1)
    {
        durability -= damage;

        if (durability <= 0)
        {
            if (type == typeOf.tool)
            {
                ClickControl cc = GameObject.Find("Player").GetComponent<ClickControl>();
                cc.inventory.RemoveItem(this, cc.inventory.tools);
                cc.PrintInventory();
                cc.MoveInv(true);
                Object.Destroy(gObject);
                if (chunk != null)
                    chunk.objects.Remove(this);
            }
            else
            {
                ObjectPooler.Instance.poolDictionary[oName].Enqueue(gObject);
                gObject.transform.position = new Vector3(10000000, 10000000, 10000000);
                chunk.objects.Remove(this);
            }
        }
    }

    public float PercentDamaged()
    {
        return (float)(((float)maxDurability - (float)durability)/ (float)maxDurability);
    }
}