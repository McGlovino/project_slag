﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_TumbleWeed : Item
{
    public ITM_TumbleWeed(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Tumble Weed", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Tumble Weed";
        type = Item.typeOf.select;
        chunk = chunkT;

        gObject.transform.GetChild(0).GetComponent<TumbleWeed>().currentChunk = chunk;
        gObject.transform.GetChild(0).GetComponent<TumbleWeed>().thisItem = this;
    }

    public ITM_TumbleWeed()
    {
        oName = "Tumble Weed";
        type = Item.typeOf.select;
    }
    public static Vector3 GenerateLocation(Transform on)
    {
        float minX = on.position.x - on.localScale.x * 10 / 2;
        float maxX = on.position.x + on.localScale.x * 10 / 2;
        float minZ = on.position.z - on.localScale.z * 10 / 2;
        float maxZ = on.position.z + on.localScale.z * 10 / 2;
        return new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
    }
}
