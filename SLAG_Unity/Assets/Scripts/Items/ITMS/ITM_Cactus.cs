﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Cactus : Item
{
    public ITM_Cactus(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Cactus", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Cactus";
        type = Item.typeOf.collect;
        chunk = chunkT;
    }
    public ITM_Cactus()
    {
        oName = "Cactus";
        type = Item.typeOf.collect;
    }


    public override Item OtherCollect(Item selected)
    {
        if (!gObject.GetComponent<Collect>().hasCollected)
        {
            gObject.GetComponent<Collect>().CollectObj();
            return new ITM_PinkFlower();
        }
        else
            return null;
    }
}
