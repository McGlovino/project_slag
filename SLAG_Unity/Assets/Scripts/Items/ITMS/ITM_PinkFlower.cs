﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_PinkFlower : Item
{
    public ITM_PinkFlower(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Pink Flower", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Pink Flower";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }

    public ITM_PinkFlower()
    {
        oName = "Pink Flower";
        type = Item.typeOf.pickup;
    }
}
