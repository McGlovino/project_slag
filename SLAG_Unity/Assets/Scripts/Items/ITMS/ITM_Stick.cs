﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Stick : Item
{
    public ITM_Stick(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Stick", pos, Quaternion.identity, chunkT.gObject.transform);
        //Debug.Log(chunk.gObject.name);
        spawnPos = pos;
        oName = "Stick";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }

    public ITM_Stick()
    {
        oName = "Stick";
        type = Item.typeOf.pickup;
    }
}
