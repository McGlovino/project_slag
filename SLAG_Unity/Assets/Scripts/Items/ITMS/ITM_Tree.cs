﻿using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Tree : Item
{    
    public ITM_Tree(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Tree", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Tree";
        type = Item.typeOf.collect;
        chunk = chunkT;

        durability = Random.Range(4, 11);
    }

    public ITM_Tree()
    {
        oName = "Tree";
        type = Item.typeOf.collect;
    }

    public override Item OtherCollect(Item selected)
    {
        int dropped = 0;
        Drop drop = gObject.GetComponent<Drop>();
        if (drop.hasDropped && !drop.hasDroppedOther)
        {
            //default (overwritten with positions of objects on tree
            int dropping = 3;
            Vector3 pos = new Vector3();

            if (drop.drops.Count > 0)
                dropping = drop.drops.Count;

            for (int i = 0; i < dropping; i++)
            {
                if (drop.drops.Count > 0)
                    pos = new Vector3(drop.drops[i].position.x, drop.dropsPos[i].y, drop.drops[i].position.z); // - (drop.drops[i].forward * 0.5f);
                else
                    pos = (gObject.transform.position + new Vector3(0, 1f, 0)) + (gObject.transform.forward * 0.4f) + (gObject.transform.right * (i == 0 ? 0 : (i == 1 ? -0.5f : 0.5f)));

                if (Random.Range(1, dropping) > 1 || (dropped == 0 && i == dropping - 1))
                {
                    Item temp = drop.DropOtherObj(new ITM_Stick(pos, chunk));
                    temp.gObject.GetComponent<MeshRenderer>().material = (Material)Resources.Load("Materials/Brown");
                    foreach (MeshRenderer m in temp.gObject.GetComponentsInChildren<MeshRenderer>())
                        m.material = (Material)Resources.Load("Materials/Brown");
                    chunk.objects.Add(temp);

                    dropped++;
                }
            }
            drop.hasDroppedOther = true;
        }
        else if(drop.hasDropped && drop.hasDroppedOther && (selected!=null && selected.toolType == toolTypeOf.Axe))
        {
            Damage();
            selected.Damage();
            //selected.gObject.GetComponent<Animation>().Play("Use Tool");
            return new ITM_Wood();
        }
        else
            gObject.GetComponent<Drop>().DropObj();

        return null;
    }
}
