﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Bush : Item
{
    public ITM_Bush(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Bush", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Bush";
        type = Item.typeOf.select;
        chunk = chunkT;
    }

    public ITM_Bush()
    {
        oName = "Bush";
        type = Item.typeOf.select;
    }
}
