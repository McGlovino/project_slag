﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_RedApple : Item
{
    public ITM_RedApple(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Red Apple", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Red Apple";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }
    public ITM_RedApple()
    {
        oName = "Red Apple";
        type = Item.typeOf.pickup;
    }

}
