﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Stone : Item
{
    public ITM_Stone(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Stone", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Stone";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }
    public ITM_Stone()
    {
        oName = "Stone";
        type = Item.typeOf.pickup;
    }
}
