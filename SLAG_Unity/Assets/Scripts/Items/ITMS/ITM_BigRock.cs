﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_BigRock : Item
{
    public ITM_BigRock(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Big Rock", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Big Rock";
        type = Item.typeOf.collect;
        chunk = chunkT;

        durability = Random.Range(8, 16);
    }

    public ITM_BigRock()
    {
        oName = "Big Rock";
        type = Item.typeOf.select;
    }

    public override Item OtherCollect(Item selected)
    {
        if (selected!= null && selected.toolType == toolTypeOf.PickAxe)
        {
            Damage();
            selected.Damage();
            //selected.gObject.GetComponent<Animation>().Play("Use Tool");
            return new ITM_Stone();
        }
        return null;
    }
}
