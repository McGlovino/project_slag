﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Grass : Item
{
    public ITM_Grass(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Grass", pos, Quaternion.identity, chunkT.gObject.transform);
        //Debug.Log(chunk.gObject.name);
        spawnPos = pos;
        oName = "Grass";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }

    public ITM_Grass()
    {
        oName = "Grass";
        type = Item.typeOf.pickup;
    }

    public static Vector3 GenerateLocation(Vector3 from, float blur)
    {
        float minX = from.x - blur;
        float maxX = from.x + blur;
        float minZ = from.z - blur;
        float maxZ = from.z + blur;
        return new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
    }
}
