﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_PickWood : Item
{
    public ITM_PickWood()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/PickWood")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Wood Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;

        durability = Constants.woodDurability;
        maxDurability = Constants.woodDurability;
    }

    public ITM_PickWood(bool other)
    {
        oName = "Wood Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;
    }
}
