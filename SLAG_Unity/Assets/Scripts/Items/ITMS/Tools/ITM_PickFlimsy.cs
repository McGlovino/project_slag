﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_PickFlimsy : Item
{
    public ITM_PickFlimsy()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/PickFlimsy")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Flimsy Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;

        durability = Constants.flimsyDurability;
        maxDurability = Constants.flimsyDurability;
    }

    public ITM_PickFlimsy(bool other)
    {
        oName = "Flimsy Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;
    }
}
