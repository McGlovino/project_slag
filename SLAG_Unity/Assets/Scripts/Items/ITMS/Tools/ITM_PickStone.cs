﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_PickStone : Item
{
    public ITM_PickStone()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/PickStone")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Stone Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;

        durability = Constants.stoneDurability;
        maxDurability = Constants.stoneDurability;
    }

    public ITM_PickStone(bool other)
    {
        oName = "Stone Pick";
        type = typeOf.tool;
        toolType = toolTypeOf.PickAxe;
    }
}
