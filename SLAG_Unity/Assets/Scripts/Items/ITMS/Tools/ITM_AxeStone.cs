﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_AxeStone : Item
{
    public ITM_AxeStone()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/AxeStone")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Stone Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;

        durability = Constants.stoneDurability;
        maxDurability = Constants.stoneDurability;
    }

    public ITM_AxeStone(bool other)
    {
        oName = "Stone Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;
    }
}
