﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Torch : Item
{
    public ITM_Torch()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/Torch")
            , toolPos.position, toolPos.rotation, toolPos);
        oName = "Torch";
        type = typeOf.tool;
        toolType = toolTypeOf.Torch;

        durability = 3 * 60;
        maxDurability = 3 * 60;

        gObject.GetComponent<Torch>().thisItem = this;
    }

    public ITM_Torch(bool other)
    {
        oName = "Torch";
        type = typeOf.tool;
        toolType = toolTypeOf.Torch;

    }

    public override void Damage(float damage = 1)
    {
        if(durability <= 0 && damage < 0) // adding duability, add flame back
            gObject.GetComponent<Torch>().RemoveFlame(true);

        durability -= damage;

        if (durability <= 0)
        {
            ClickControl cc = GameObject.Find("Player").GetComponent<ClickControl>();
            cc.inventory.RemoveItem(this, cc.inventory.tools);
            cc.PrintInventory();
            cc.MoveInv(true);
            if (chunk == null)
                Object.Destroy(gObject);
            else
                gObject.GetComponent<Torch>().RemoveFlame();
        }
    }
}
