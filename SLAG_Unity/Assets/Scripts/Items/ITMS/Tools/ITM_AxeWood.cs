﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_AxeWood : Item
{
    public ITM_AxeWood()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/AxeWood")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Wood Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;

        durability = Constants.woodDurability;
        maxDurability = Constants.woodDurability;
    }

    public ITM_AxeWood(bool other)
    {
        oName = "Wood Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;
    }
}