﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_AxeFlimsy : Item
{
    public ITM_AxeFlimsy()
    {
        Transform toolPos = GameObject.Find("Player").transform.GetChild(0).transform.GetChild(0);
        gObject = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/Tools/AxeFlimsy")
            , toolPos.position, toolPos.rotation, toolPos);
        //ObjectPooler.Instance.GetFromQueue("WoodenPick", pos, Quaternion.identity, chunkT.gObject.transform);
        //spawnPos = pos;
        oName = "Flimsy Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;

        durability = Constants.flimsyDurability;
        maxDurability = Constants.flimsyDurability;
    }

    public ITM_AxeFlimsy(bool other)
    {
        oName = "Flimsy Axe";
        type = typeOf.tool;
        toolType = toolTypeOf.Axe;
    }
}