﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_BigMushroom : Item
{
    public ITM_BigMushroom(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Big Mushroom", pos, Quaternion.identity, chunkT.gObject.transform);
        //Debug.Log(chunk.gObject.name);
        spawnPos = pos;
        oName = "Big Mushroom";
        type = Item.typeOf.pickup;
        chunk = chunkT;
    }

    public ITM_BigMushroom()
    {
        oName = "Big Mushroom";
        type = Item.typeOf.pickup;
    }

    public static Vector3 GetRandomPosition(Transform chunk)
    {
        int child = 999999;
        while (child > chunk.childCount || chunk.GetChild(child).gameObject.layer != LayerMask.NameToLayer("Floor"))
            child = Random.Range(0, chunk.childCount);

        float minX = chunk.GetChild(child).position.x - chunk.GetChild(child).localScale.x * 10 / 2;
        float minZ = chunk.GetChild(child).position.z - chunk.GetChild(child).localScale.z * 10 / 2;
        float maxX = chunk.GetChild(child).position.x + chunk.GetChild(child).localScale.x * 10 / 2;
        float maxZ = chunk.GetChild(child).position.z + chunk.GetChild(child).localScale.z * 10 / 2;

        return new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
    }

    public static Vector3 GetRandomPositionSection(Transform section)
    {
        float minX = section.position.x - section.localScale.x * 10 / 2;
        float minZ = section.position.z - section.localScale.z * 10 / 2;
        float maxX = section.position.x + section.localScale.x * 10 / 2;
        float maxZ = section.position.z + section.localScale.z * 10 / 2;

        return new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
    }
}
