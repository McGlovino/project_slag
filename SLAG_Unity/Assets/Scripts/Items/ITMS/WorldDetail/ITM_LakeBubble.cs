﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_LakeBubble : Item
{
    public ITM_LakeBubble(Vector3 pos, Chunk chunkT)
    {
        float posY = Random.Range(-0.1f, -0.05f);

        Vector3 spawnPosT = new Vector3(pos.x, posY, pos.z);
        gObject = ObjectPooler.Instance.GetFromQueue("Lake Bubble", spawnPosT, Quaternion.identity, chunkT.gObject.transform);

        float random_size = Random.Range(0.1f, 0.8f);
        gObject.transform.localScale = new Vector3(random_size, 1, random_size);

        spawnPos = spawnPosT;
        oName = "Lake Bubble";
        type = Item.typeOf.nothing;
        chunk = chunkT;

        SetColour(gObject);
    }

    public static void SetColour(GameObject gObjectT)
    {
        int random = Random.Range(0, 2);
        gObjectT.GetComponentInChildren<Renderer>().material =
            (random == 0 ? (Material)Resources.Load("Materials/Swamp Light") : (Material)Resources.Load("Materials/Swamp Dark"));
    }
}
