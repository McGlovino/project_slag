﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Moss : Item
{
    public ITM_Moss(Vector3 pos, Chunk chunkT, bool colour = false)
    {
        Vector3 spawnPosT = new Vector3(pos.x, Random.Range(0.02f, 0.06f), pos.z);
        gObject = ObjectPooler.Instance.GetFromQueue("Moss", spawnPosT, Quaternion.identity, chunkT.gObject.transform);

        int random_size = Random.Range(0, 3);
        float size = random_size == 0 ? 0.1f : random_size == 1 ? 0.2f : 0.3f;
        gObject.transform.GetChild(0).localScale = new Vector3(size, size, 1);

        spawnPos = spawnPosT;
        oName = "Moss";
        type = Item.typeOf.nothing;
        chunk = chunkT;

        if (colour)
            SetColourGreen(gObject);
        else
            SetColourGrey(gObject);
    }

    public static void SetColourGreen(GameObject gObjectT)
    {
        gObjectT.GetComponentInChildren<Renderer>().material = 
            (Random.Range(0, 2) > 0 ? (Material)Resources.Load("Materials/Green") : (Material)Resources.Load("Materials/Dark Green"));
    }
    public static void SetColourGrey(GameObject gObjectT)
    {
        gObjectT.GetComponentInChildren<Renderer>().material = 
            (Random.Range(0, 2) > 0 ? (Material)Resources.Load("Materials/Dark Grey") : (Material)Resources.Load("Materials/Dark Grey 2"));
    }
}
