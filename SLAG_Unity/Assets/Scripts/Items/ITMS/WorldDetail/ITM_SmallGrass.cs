﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_SmallGrass : Item
{
    public ITM_SmallGrass(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Small Grass", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Small Grass";
        type = Item.typeOf.nothing;
        chunk = chunkT;
    }

    public ITM_SmallGrass()
    {
        oName = "Small Grass";
        type = Item.typeOf.nothing;
    }
}
