﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Clover : Item
{
    public ITM_Clover(Vector3 pos, Chunk chunkT)
    {
        int rotMod = Random.Range(0, 1);

        gObject = ObjectPooler.Instance.GetFromQueue("Clover", pos, new Quaternion(0, rotMod, 0, 1), chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Clover";
        type = Item.typeOf.nothing;
        chunk = chunkT;
    }

    public ITM_Clover()
    {
        oName = "Clover";
        type = Item.typeOf.nothing;
    }
}
