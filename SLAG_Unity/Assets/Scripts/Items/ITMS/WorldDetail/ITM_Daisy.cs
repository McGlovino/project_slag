﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Daisy : Item
{
    public ITM_Daisy(Vector3 pos, Chunk chunkT)
    {
        int rotMod = Random.Range(0, 1);

        gObject = ObjectPooler.Instance.GetFromQueue("Daisy", pos, new Quaternion(0, rotMod, 0, 1), chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Daisy";
        type = Item.typeOf.nothing;
        chunk = chunkT;
    }

    public ITM_Daisy()
    {
        oName = "Daisy";
        type = Item.typeOf.nothing;
    }
}
