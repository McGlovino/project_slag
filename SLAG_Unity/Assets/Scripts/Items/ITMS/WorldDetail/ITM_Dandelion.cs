﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Dandelion : Item
{
    public ITM_Dandelion(Vector3 pos, Chunk chunkT)
    {
        int rotMod = Random.Range(0, 1);

        gObject = ObjectPooler.Instance.GetFromQueue("Dandelion", pos, new Quaternion(0, rotMod, 0, 1), chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Dandelion";
        type = Item.typeOf.nothing;
        chunk = chunkT;
    }

    public ITM_Dandelion()
    {
        oName = "Dandelion";
        type = Item.typeOf.nothing;
    }
}
