﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Rock : Item
{
    public ITM_Rock(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Rock", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Rock";
        type = Item.typeOf.collect;
        chunk = chunkT;

        durability = Random.Range(4, 8);
    }

    public ITM_Rock()
    {
        oName = "Rock";
        type = Item.typeOf.select;
    }

    public override Item OtherCollect(Item selected)
    {
        if (selected != null && selected.toolType == toolTypeOf.PickAxe)
        {
            Damage();
            selected.Damage();
            //selected.gObject.GetComponent<Animation>().Play("Use Tool");
            return new ITM_Stone();
        }
        return null;
    }
}
