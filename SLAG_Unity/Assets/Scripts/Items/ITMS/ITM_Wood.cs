﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_Wood : Item
{
    public ITM_Wood(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Wood", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Wood";
        type = Item.typeOf.pickup;
        chunk = chunkT;

        canPlace = false;
    }
    public ITM_Wood()
    {
        oName = "Wood";
        type = Item.typeOf.pickup;

        canPlace = false;
    }
}
