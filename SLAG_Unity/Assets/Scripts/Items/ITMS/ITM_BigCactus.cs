﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITM_BigCactus : Item
{
    public ITM_BigCactus(Vector3 pos, Chunk chunkT)
    {
        gObject = ObjectPooler.Instance.GetFromQueue("Big Cactus", pos, Quaternion.identity, chunkT.gObject.transform);
        spawnPos = pos;
        oName = "Big Cactus";
        type = Item.typeOf.collect;
        chunk = chunkT;
        //otherCollect = new ITM_PinkFlower();
    }

    public ITM_BigCactus()
    {
        oName = "Big Cactus";
        type = Item.typeOf.collect;
    }

    public override Item OtherCollect(Item selected)
    {
        if (!gObject.GetComponent<Collect>().hasCollected)
        {
            gObject.GetComponent<Collect>().CollectObj();
            return new ITM_PinkFlower();
        }
        else
            return null;
    }
}
