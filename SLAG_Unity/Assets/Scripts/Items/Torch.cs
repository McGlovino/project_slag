﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Torch : MonoBehaviour
{
    public GameObject flame;

    [HideInInspector]
    public Item thisItem;
    float startIntenstity;
    float startRange;

    bool on = true;

    private void Start()
    {
        startIntenstity = GetComponentInChildren<Light>().intensity;
        startRange = GetComponentInChildren<Light>().range;
    }
    void Update()
    {
        if (GetComponentInChildren<Light>() != null && GetComponentInChildren<Light>().enabled == true)
        {
            if (on)
            {
                if (flame.activeSelf == false)
                    flame.SetActive(true);

                thisItem.Damage(Time.deltaTime);

                GetComponentInChildren<Light>().intensity
                    = Functions.RangeChange(thisItem.durability, 0, thisItem.maxDurability, startIntenstity * 0.6f, startIntenstity);

                GetComponentInChildren<Light>().range
                    = Functions.RangeChange(thisItem.durability, 0, thisItem.maxDurability, startRange * 0.6f, startRange);
            }
            else if (!on && GetComponentInChildren<Light>().intensity <= 0 && GetComponentInChildren<Light>().range <= 0 && flame.activeSelf == true)
            {
                flame.SetActive(false);
            }
            else if (!on && flame.activeSelf == true)
            {
                if (GetComponentInChildren<Light>().intensity > 0)
                    GetComponentInChildren<Light>().intensity -= Time.deltaTime * 2;

                if (GetComponentInChildren<Light>().range > 0)
                    GetComponentInChildren<Light>().range -= Time.deltaTime * 30;
            }
        }
    }

    public void RemoveFlame(bool reverse = false)
    {
        on = reverse;
    }
}
