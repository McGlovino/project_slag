﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class Bounce : MonoBehaviour
{
    Transform parent;
    PlayerMovement PM;

    float minBounceUp = 0.25f;

    float bounceDistance = 1f;

    private void Start()
    {
        parent = transform.parent;
        PM = parent.gameObject.GetComponent<PlayerMovement>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bounce"))
        {
            if (parent.position.y >= minBounceUp)
            {
                PM.bouncing = true;
                StartCoroutine(BounceUp());
            }
            else
            {
                PM.bouncing = true;
                StartCoroutine(BounceBack());
            }
        }
    }

    IEnumerator BounceBack()
    {
        PM.goingDown = false;
        PM.jumping = false;

        float moved = 0;
        Vector3 ogPos = this.transform.position;

        while (moved < bounceDistance)
        {
            moved = (PM.walk(-PM.moveDir) ? Vector3.Distance(ogPos, parent.position) : bounceDistance + 1);
            yield return new WaitForEndOfFrame();
        }
        PM.jump.y = 0;
        PM.moveDir = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(0.1f);
        PM.bouncing = false;
    }

    IEnumerator BounceUp()
    {
        PM.goingDown = false;
        float newbounceHeight = (PM.jumpHeight * 1.25f) + parent.position.y;
        while(parent.position.y < newbounceHeight)
        {
            PM.Jump(true, (PM.jump.y <= 0 ? 4f : 1));
            yield return new WaitForEndOfFrame();
        }
        PM.goingDown = true;
        PM.bouncing = false;
    }
}
