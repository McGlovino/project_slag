﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Copy meshes from children into the parent's Mesh.
// CombineInstance stores the list of meshes.
//These are combined and assigned to the attached Mesh.

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MergeMesh : MonoBehaviour
{
    //public bool onstart = true;

    public bool tagSpecific = false;
    public bool meshCollider = false;
    public float waitTime = 0;

    void Start()
    {
        StartCoroutine(merge());
    }

    IEnumerator merge()
    {
        yield return new WaitForEndOfFrame(); // avoid the occasional error at start hopefully
        yield return new WaitForSeconds(waitTime);

        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;

        //MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
        List<MeshFilter> meshFilters = GetMeshFilters(transform);

        CombineInstance[] combine = new CombineInstance[meshFilters.Count];

        for (int i = 0; i < meshFilters.Count; i++)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
        }
        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        transform.gameObject.SetActive(true);

        transform.rotation = oldRot;
        transform.position = oldPos;

        if (meshCollider)
            gameObject.AddComponent<MeshCollider>();
        else if (gameObject.tag == "NoWalk")
            gameObject.AddComponent<BoxCollider>();
    }

    List<MeshFilter> GetMeshFilters(Transform current)
    {
        List<MeshFilter> meshFiltersTemp = new List<MeshFilter>();
        foreach (Transform child in current)
        {
            if ((!tagSpecific || (tagSpecific && transform.tag == child.tag)) && child.GetComponent<MeshFilter>() != null)
                meshFiltersTemp.Add(child.GetComponent<MeshFilter>());
            if (child.childCount > 0)
                meshFiltersTemp.AddRange(GetMeshFilters(child));
        }
        return meshFiltersTemp;
    }
}