﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class TumbleWeed : MonoBehaviour
{
    float[] speeds = {0,0,0};
    public float minSpeed = -30.0f;
    public float maxSpeed = 30.0f;
    public Chunk currentChunk;
    public Item thisItem;
    bool destroying = false;
    bool positiveLast;
    LookAtCamera2 lookAtCamera;

    float chunkCheckCounter = 0;

    void Start()
    {
        for (int i = 0; i < speeds.Length; i++)
            speeds[i] = Random.Range(minSpeed, maxSpeed);

        positiveLast = (Random.Range(-1, 1) >= 0 ? true : false);

        lookAtCamera = Camera.main.GetComponent<LookAtCamera2>();
    }

    void LateUpdate()
    {
		if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            for (int i = 0; i < speeds.Length; i++)
            {
                transform.GetChild(i).Rotate(0, 0, speeds[i] * Time.deltaTime * 10);//rotate
                if (Random.Range(0, 3) == 0)//dont always change
                    speeds[i] += Random.Range((positiveLast ? -0.05f : -0.1f), (positiveLast ? 0.1f : 0.05f)); //randomly change over time
                speeds[i] = Mathf.Clamp(speeds[i], Mathf.Clamp(minSpeed, speeds[0] - 0.3f, minSpeed), Mathf.Clamp(maxSpeed, speeds[0] + 0.3f, maxSpeed)); //Keep front sections near speed of main
            }
            positiveLast = (speeds[0] >= 0 ? true : false);
            transform.parent.Translate(new Vector3((-speeds[0] / 2) * Time.deltaTime, 0, 0));

            lookAtCamera.LookObject(thisItem.gObject);

            if (chunkCheckCounter >= 10 && !destroying)
            {
                if (currentChunk == null)
                    Debug.Log("No Chunk: TumbleWeed");

                Chunk tempChunk = currentChunk;
                currentChunk = ChunkActive.Instance.GetChunk(transform.position);
                /*if (currentChunk == null) //Add it to the place it would go to, removed this as not necessarily desert
                {
                    float closestDistance = 999999;
                    Chunk closestChunk = null;
                    foreach (Chunk c in ChunkActive.Instance.getRadius(tempChunk))
                    {
                        float dist = Vector3.Distance(transform.parent.position, c.gObject.transform.position);
                        if (dist < closestDistance)
                        {
                            closestDistance = dist;
                            closestChunk = c;
                        }
                    }
                    thisItem.chunk = closestChunk;
                    tempChunk.objects.Remove(thisItem);
                    closestChunk.objects.Add(thisItem);
                    thisItem.spawnPos = transform.parent.position;
                    this.gameObject.SetActive(false);
                    return;
                }*/
                if (currentChunk == null || currentChunk.gObject.transform.GetChild(0).GetComponent<DesertGenerate>() == null || Physics.Raycast(transform.position, Vector3.down, 30f, LayerMask.GetMask("Water")))
                {
                    StartCoroutine(DestroySelf(tempChunk));
                    return;
                }

                if (thisItem == null)
                    Debug.Log("No Item: TumbleWeed");
                else if (tempChunk != currentChunk)
                {
                    thisItem.chunk = currentChunk;
                    tempChunk.objects.Remove(thisItem);
                    currentChunk.objects.Add(thisItem);
                    thisItem.spawnPos = transform.parent.position;
                }
                chunkCheckCounter = 0;
            }
            chunkCheckCounter++;
        }
    }

    IEnumerator DestroySelf(Chunk tempchunkT)
    {
        destroying = true;
        if (currentChunk != null)
            currentChunk.objects.Remove(thisItem);
        tempchunkT.objects.Remove(thisItem);
        while (transform.parent.localScale.x > 0)
        {
            speeds[0] = Mathf.Clamp(speeds[0], -5, 5);
            transform.parent.localScale = new Vector3(transform.parent.localScale.x - 0.035f, transform.parent.localScale.y -0.035f, transform.parent.localScale.z -0.035f);
            yield return new WaitForEndOfFrame();
        }
        Destroy(transform.parent.gameObject);
    }
}
