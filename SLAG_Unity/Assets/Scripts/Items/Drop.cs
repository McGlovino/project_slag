﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour
{
    [HideInInspector]
    public bool hasDropped = false;
    [HideInInspector]
    public bool hasDroppedOther = false;

    [HideInInspector]
    public List<Transform> drops;
    public List<Vector3> dropsPos;

    void Start()
    {
        drops = new List<Transform>();
        foreach (Transform child in this.transform.GetChild(0))
        {
            if (child.tag == "Drop")
            {
                drops.Add(child);
                dropsPos.Add(child.position);
            }
        }
    }
    public void DropObj()
    {
        if (!hasDropped)
        {
            GetComponent<Animation>().Play("Fruit Fall");
            hasDropped = true;
        }
    }

    public Item DropOtherObj(Item item, float forwardFall = 0f)
    {
        if (!hasDroppedOther)
            StartCoroutine(item.Fall(forwardFall));
        return item;
    }

    IEnumerator spawnObj()
    {
        foreach(Transform child in drops)
        {
            ChunkActive.Instance.currentChunk.objects.Add(new ITM_RedApple(new Vector3(child.position.x, 0, child.position.z), ChunkActive.Instance.currentChunk));
            yield return new WaitForSeconds(0.02f);
            child.gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(Constants.fruitRegrow);
        foreach (Transform child in drops)
        {
            child.gameObject.SetActive(true);   
            GetComponent<Animation>().Play("Return Fruit");
        }
        hasDropped = false;
    }
}
