﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesertGenerate : MonoBehaviour
{
    Chunk chunk;

    //short grass
    float[] sg_Sparseness = { 30, 26, 24 };//ITS IN REVERSE OK T_T density
    float[] sg_PatchSmallness = { 2.5f, 2.2f, 1.8f };//ITS IN REVERSE OK T_T patch size
    float[] sg_EdgeBlur = { 2.5f, 2.5f, 2.5f };
    float[] sg_Gap = { 0.8f, 0.72f, 0.67f };
    int[] sg_MinSpawn = { 1, 1, 1 };
    int[] sg_MaxSpawn = { 2, 3, 4 };

    //Big Cacti
    float bc_Sparseness = 70;//ITS IN REVERSE OK T_T density
    float bc_PatchSmallness = 1.5f;//ITS IN REVERSE OK T_T patch size
    float bc_EdgeBlur = 5f;
    float bc_Gap = 0.85f;
    int bc_MinSpawn = 0;
    int bc_MaxSpawn = 2;

    //cacti
    float b_Sparseness = 70;//ITS IN REVERSE OK T_T density
    float b_PatchSmallness = 1.5f;//ITS IN REVERSE OK T_T patch size
    float b_EdgeBlur = 2f;
    float b_Gap = 0.8f;
    int b_MinSpawn = 0;
    int b_MaxSpawn = 2;

    //TumbleWeed
    float counter, timeToWait = 0;
    float minS = 45f;
    float maxS = 200f;

    float grassFromTree_EdgeBlur = 3f;

    bool spawnedSC, spawnedBC, spawnedWD = false;

    public bool firstRun = false;

    bool needs_to_regenerate_WD = false;
    bool needs_to_regenerate_WD_Live = false;
    WorldDetail last_WD = WorldDetail.High;

    private void Awake()
    {
        Settings.Instance.OnWorldDetailChanged += OnWorldDetailChanged;
    }

    void Start()
    {
        timeToWait = Random.Range(minS, maxS);

        chunk = ChunkActive.Instance.FindChunk(transform.parent.gameObject);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer != 10)
                continue;

            Spawn(transform.GetChild(i));
        }
    }

    private void LateUpdate()
    {
        if (this.gameObject.activeSelf == true)
        {
            if (counter >= timeToWait)
            {
                SpawntumbleWeed(transform.GetChild(Random.Range(0, transform.childCount)));
                counter = 0;
                timeToWait = Random.Range(minS, maxS);
            }
            counter += Time.deltaTime;
        }
    }

    private void OnEnable()
    {
        if (last_WD != Settings.Instance.WorldDetailSet)
        {
            last_WD = Settings.Instance.WorldDetailSet;
            regenerateWorldDetail(true);
        }
        StartCoroutine(Respawn());
    }

    private void OnDisable()
    {
        ReturnToQueue();
    }
    private void Update()
    {
        if (needs_to_regenerate_WD_Live && GameStateManager.Instance.CurrentGameState != GameState.Paused)
        {
            spawnedWD = false;
            regenerateWorldDetail(false);
            needs_to_regenerate_WD_Live = false;
        }
    }

    private void OnWorldDetailChanged(WorldDetail newWorldDetail)
    {
        needs_to_regenerate_WD_Live = true;
    }

    public void regenerateWorldDetail(bool fromEnable)
    {
        if (!fromEnable)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.layer != 10)
                    continue;
                generateWorldDetail(transform.GetChild(i), true);
            }
        }
        else
            needs_to_regenerate_WD = true;

    }


    IEnumerator Respawn()
    {
        if (spawnedSC && spawnedBC)
        {
            spawnedSC = false; spawnedBC = false;
            int done = 0;
            for (int i = 0; i < chunk.objects.Count; i++)
            {
                if (chunk.objects[i] != null)
                {
                    chunk.objects[i].gObject = ObjectPooler.Instance.GetFromQueue(chunk.objects[i].oName, chunk.objects[i].spawnPos, Quaternion.identity, transform.parent);
                    chunk.objects[i].gObject.SetActive(true);
                    if(chunk.objects[i].oName == "Tumble Weed")
                    {
                        chunk.objects[i].gObject.GetComponentInChildren<TumbleWeed>().currentChunk = chunk;
                        chunk.objects[i].gObject.GetComponentInChildren<TumbleWeed>().thisItem = chunk.objects[i];
                    }
                }

                done++;
                if (done >= 20)
                {
                    yield return new WaitForEndOfFrame();
                    done = 0;
                }
            }
            spawnedSC = true; spawnedBC = true;
        }
        if (spawnedWD)
        {
            spawnedWD = false;
            if (needs_to_regenerate_WD)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).gameObject.layer != 10)
                        continue;
                    generateWorldDetail(transform.GetChild(i), true);
                }
                needs_to_regenerate_WD = false;
            }
            else
            {
                int done = 0;
                foreach (Item i in chunk.worldDetail)
                {
                    i.gObject = ObjectPooler.Instance.GetFromQueue(i.oName, i.spawnPos, Quaternion.identity, transform.parent);
                    i.gObject.SetActive(true);

                    done++;
                    if (done >= 20)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
                spawnedWD = true;
            }
        }
    }

    void ReturnToQueue(bool justWD = false)
    {
        if (spawnedSC && spawnedBC && !justWD)
        {
            foreach (Item i in chunk.objects)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
        if (spawnedWD || justWD)
        {
            foreach (Item i in chunk.worldDetail)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
    }
    void generateWorldDetail(Transform spawnOn, bool reset = false)
    {

        if (reset)
        {
            ReturnToQueue(true);
            chunk.worldDetail = new List<Item>();
        }

        int detail_level = (int)Settings.Instance.WorldDetailSet - 1;

        if (detail_level < 0)
        {
            spawnedWD = true;
            return;
        }

        //Short Grass
        StartCoroutine(PerlinNoise.GenerateTexture2(sg_PatchSmallness[detail_level], spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, sg_Sparseness[detail_level], sg_Gap[detail_level], firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, sg_EdgeBlur[detail_level], sg_MinSpawn[detail_level], sg_MaxSpawn[detail_level], firstRun
                    , (returnItems) => StartCoroutine(GenerateShortGrass(returnItems))))))));
    }

    void Spawn(Transform spawnOn)
    {
        //TumbleWeed
        GenerateTumbleWeedStart(spawnOn);

        generateWorldDetail(spawnOn);
        //Cactus
        StartCoroutine(PerlinNoise.GenerateTexture2(b_PatchSmallness, spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, b_Sparseness, b_Gap, firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, b_EdgeBlur, b_MinSpawn, b_MaxSpawn, firstRun
                    , (returnItems) => StartCoroutine(GenerateCacti(returnItems))))))));
        //BigCactus
        StartCoroutine(PerlinNoise.GenerateTexture2(bc_PatchSmallness, spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, bc_Sparseness, bc_Gap, firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, bc_EdgeBlur, bc_MinSpawn, bc_MaxSpawn, firstRun
                    , (returnItems) => StartCoroutine(GenerateBigCacti(returnItems))))))));
    }

    void GenerateTumbleWeedStart(Transform spawnOn)
    {
        if (Random.Range(0, 2) == 1)
            SpawntumbleWeed(spawnOn);
    }
    void SpawntumbleWeed(Transform spawnOn)
    {
        Vector3 spawnLocation = ITM_TumbleWeed.GenerateLocation(spawnOn);
        chunk.objects.Add(new ITM_TumbleWeed(spawnLocation, chunk));
    }

    IEnumerator GenerateShortGrass(List<Vector3> returned)
    {
        spawnedWD = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.worldDetail.Add(new ITM_SmallGrass(I, chunk));

            if (!firstRun)
                done++;
            if (done >= 20)
            {
                yield return new WaitForEndOfFrame();
                done = 0;

                if (needs_to_regenerate_WD_Live)
                    break;
            }
        }
        spawnedWD = true;
    }

    IEnumerator GenerateBigCacti(List<Vector3> returned)
    {
        spawnedBC = false;
        foreach (Vector3 I in returned)
        {
            chunk.objects.Add(new ITM_BigCactus(I, chunk));

            for (int j = 0; j < Random.Range(0, 3); j++)
            {
                Vector3 grassSpawnPos = ITM_Grass.GenerateLocation(I, grassFromTree_EdgeBlur);
                if (Functions.isGrounded(grassSpawnPos))
                    chunk.objects.Add(new ITM_Grass(grassSpawnPos, chunk));
            }
            if (!firstRun)
                yield return new WaitForEndOfFrame();
        }
        spawnedBC = true;
    }

    IEnumerator GenerateCacti(List<Vector3> returned)
    {
        spawnedSC = false;
        foreach (Vector3 I in returned)
        {
            chunk.objects.Add(new ITM_Cactus(I, chunk));

            for (int j = 0; j < Random.Range(0, 2); j++)
            {
                Vector3 grassSpawnPos = ITM_Grass.GenerateLocation(I, grassFromTree_EdgeBlur);
                if (Functions.isGrounded(grassSpawnPos))
                    chunk.objects.Add(new ITM_Grass(grassSpawnPos, chunk));
            }
            if (!firstRun)
                yield return new WaitForEndOfFrame();
        }
        spawnedSC = true;
    }
}
