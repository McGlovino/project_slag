﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPooler : MonoBehaviour
{
    string[] worldDetailNames = { "Small Grass" , "Moss", "Daisy", "Dandelion", "Clover" };

    [System.Serializable]
    public class Pool
    {
        public string name;
        public GameObject obj;
        public int size;

        public Pool(string nameT, GameObject objT, int sizeT)
        {
            name = nameT;
            obj = objT;
            size = sizeT;
        }
    }

    #region Singleton
    public static ObjectPooler Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    List<Pool> pools = new List<Pool>();
    public Dictionary<string, Queue<GameObject>> poolDictionary = new Dictionary<string, Queue<GameObject>>();

    void Start()
    {
        DefinePools();
        foreach (Pool p in pools) {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < p.size; i++)
            {
                GameObject temp = Instantiate(p.obj, this.transform);
                temp.SetActive(false);
                objectPool.Enqueue(temp);
            }
            poolDictionary.Add(p.name, objectPool);
        }
    }

    void DefinePools()
    {
        pools.Add(new Pool("Small Grass", Resources.Load("Prefabs/WorldDetail/Small Grass") as GameObject, 5500));
        pools.Add(new Pool("Moss", Resources.Load("Prefabs/WorldDetail/Moss") as GameObject, 5500));
        pools.Add(new Pool("Daisy", Resources.Load("Prefabs/WorldDetail/Daisy") as GameObject, 2000));
        pools.Add(new Pool("Dandelion", Resources.Load("Prefabs/WorldDetail/Dandelion") as GameObject, 2000));
        pools.Add(new Pool("Clover", Resources.Load("Prefabs/WorldDetail/Clover") as GameObject, 2000));
        pools.Add(new Pool("Lake Bubble", Resources.Load("Prefabs/WorldDetail/Lake Bubble") as GameObject, 100));
        pools.Add(new Pool("Lake Bubble 2", Resources.Load("Prefabs/WorldDetail/Lake Bubble 2") as GameObject, 100));
        pools.Add(new Pool("Grass", Resources.Load("Prefabs/Grass") as GameObject, 1000));
        pools.Add(new Pool("Bush", Resources.Load("Prefabs/Bush") as GameObject, 250));
        pools.Add(new Pool("Tree", Resources.Load("Prefabs/Tree") as GameObject, 250));
        pools.Add(new Pool("Evergreen Tree", Resources.Load("Prefabs/Evergreen Tree") as GameObject, 250));
        pools.Add(new Pool("Rock", Resources.Load("Prefabs/Rock") as GameObject, 100));
        pools.Add(new Pool("Big Rock", Resources.Load("Prefabs/Big Rock") as GameObject, 100));
        pools.Add(new Pool("Cactus", Resources.Load("Prefabs/Cactus") as GameObject, 100));
        pools.Add(new Pool("Big Cactus", Resources.Load("Prefabs/Big Cactus") as GameObject, 100));
        pools.Add(new Pool("Mushroom", Resources.Load("Prefabs/Mushroom") as GameObject, 100));
        pools.Add(new Pool("Big Mushroom", Resources.Load("Prefabs/Big Mushroom") as GameObject, 30));
        pools.Add(new Pool("Red Apple", Resources.Load("Prefabs/Red Apple") as GameObject, 30));
        pools.Add(new Pool("Tumble Weed", Resources.Load("Prefabs/Tumble Weed") as GameObject, 50));
        pools.Add(new Pool("Pink Flower", Resources.Load("Prefabs/Pink Flower") as GameObject, 5));
        pools.Add(new Pool("Stick", Resources.Load("Prefabs/Stick") as GameObject, 5));
        pools.Add(new Pool("Stone", Resources.Load("Prefabs/Stone") as GameObject, 5));
    }

    void Update()
    {
        foreach (string q in poolDictionary.Keys)
        {
            if (worldDetailNames.Contains(q) && poolDictionary[q].Count < 800)
            {
                for (int i = 0; i < 50; i++)
                    poolDictionary[q].Enqueue(Instantiate(poolDictionary[q].Peek(), this.transform));
            }
            else if ((q == "Red Apple" || q == "Big Mushroom" || q == "Mushroom") && poolDictionary[q].Count < 20)
            {
                for (int i = 0; i < 2; i++)
                    poolDictionary[q].Enqueue(Instantiate(poolDictionary[q].Peek(), this.transform));
            }
            else if ((q == "Pink Flower" || q == "Stick" || q == "Stone") && poolDictionary[q].Count < 5)
            {
                for (int i = 0; i < 2; i++)
                    poolDictionary[q].Enqueue(Instantiate(poolDictionary[q].Peek(), this.transform));
            }
            else if (poolDictionary[q].Count < 50)
            {
                for (int i = 0; i < 5; i++)
                    poolDictionary[q].Enqueue(Instantiate(poolDictionary[q].Peek(), this.transform));
            }
        }
    }

    public GameObject GetFromQueue(string name, Vector3 pos, Quaternion rot, Transform parent)
    {
        if (poolDictionary[name].Count < 2)
            poolDictionary[name].Enqueue(Instantiate(poolDictionary[name].Peek(), this.transform));

        GameObject toSpawn = poolDictionary[name].Dequeue();
        if (!worldDetailNames.Contains(name))
            toSpawn.SetActive(true);

        toSpawn.transform.position = pos;
        toSpawn.transform.rotation = rot;
        toSpawn.transform.SetParent(parent, true);
        //toSpawn.transform.parent = parent;
        ClickControl.changeTo(toSpawn.transform, "Default");
        return toSpawn;
    }
}
