﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwampWaterBubbleGenerate : MonoBehaviour
{
    Chunk chunk;
    public int inverseFrequency = 3;

	private void Start()
	{
        chunk = transform.parent.GetComponent<SwampGenerate>().chunk;

    }
	void Update()
    {
        int random = Random.Range(0, inverseFrequency);
        if (random == 1)
		{
            SpawnBubble();
		}

        random = Random.Range(0, inverseFrequency*2);
        if (random == 1)
        {
            SpawnBubble();
        }
    }

    void SpawnBubble()
	{
		if (chunk == null)
		{
            chunk = transform.parent.GetComponent<SwampGenerate>().chunk;
        }

        float minX = transform.localScale.x * 10 / 2;
        float minZ = transform.localScale.z * 10 / 2;

        float xPos = Random.Range(-minX, minX);
        float zPos = Random.Range(-minZ, minZ);

        Vector3 pos = transform.position + new Vector3(xPos, 0, zPos);

        new ITM_LakeBubble(pos, chunk);
    }
}
