﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToQueue : MonoBehaviour
{
    public string objectName;
	public void ReturnObjToQueue()
    {
        ObjectPooler.Instance.poolDictionary[objectName].Enqueue(this.gameObject);
        this.transform.position = new Vector3(100000, 100000, 100000);
        this.gameObject.SetActive(false);
    }
}
