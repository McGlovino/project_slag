﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public List<Item> items = new List<Item>();
    //public List<GameObject> worldDetail = new List<GameObject>();
    Transform player;
    float maxDistance = 20;
    bool worldDetailToggle = true;
    int counter = 0;

    bool setInvisibleAgain = true;

    bool firstWorldDetailDone = false;

    bool worldDetailChanged = false;

    Vector3 playerLastPos;

    #region Singleton
    public static ItemManager Instance;
    private void Awake()
    {
        Instance = this;
        Settings.Instance.OnWorldDetailChanged += OnWorldDetailChanged;
    }

    #endregion

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerLastPos = player.position;
        CallSetInvisible();
    }

    void LateUpdate()
    {
		if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            if ((setInvisibleAgain && (playerLastPos != player.position || worldDetailChanged)) || !firstWorldDetailDone)
            {
                CallSetInvisible();
                firstWorldDetailDone = true;

                if(playerLastPos != player.position)
				{
                    worldDetailChanged = false;
                }
            }
            playerLastPos = player.position;
            //else                    RemoveWorldDetail();
        }
    }

    private void OnWorldDetailChanged(WorldDetail newWorldDetail)
    {
        worldDetailChanged = true;

		switch (newWorldDetail)
		{
            case WorldDetail.Low:
                maxDistance = 13;
                break;
            case WorldDetail.Mid:
                maxDistance = 15;
                break;
            case WorldDetail.High:
                maxDistance = 18;
                break;
            default:
                break;
		}
    }

    public void ToggleWorldDetail()
    {
        worldDetailToggle = !worldDetailToggle;
        setInvisibleAgain = worldDetailToggle;
        if (!worldDetailToggle) RemoveWorldDetail();
    }

    void RemoveWorldDetail()
    {
        foreach (Chunk c in ChunkActive.Instance.radiusChunks)
        {
            foreach (Item i in c.worldDetail)
                i.gObject.SetActive(false);
        }
    }

    public void CallSetInvisible()
    {
        setInvisibleAgain = false;
        StartCoroutine(setInvisible((again) => setInvisibleAgain = again));
    }

    IEnumerator setInvisible(System.Action<bool> callbackOnFinish)
    {
        if (worldDetailToggle)
        {
            List<Chunk> closeChunks = new List<Chunk>();
            foreach (Chunk c in ChunkActive.Instance.radiusChunks)
            {
                if (Vector3.Distance(c.gObject.transform.position, player.position) < maxDistance * 1.75f)
                    closeChunks.Add(c);
            }
            int done = 0;
            foreach (Chunk c in closeChunks)
            {
                for (int i = 0; i < c.worldDetail.Count; i++)//Gameobject I in c.worldDetail)
                {
                    if (!worldDetailToggle) yield break;

                    if (c.worldDetail[i].gObject == null || c.worldDetail[i].gObject.GetComponent<Transform>() == null || (c.worldDetail[i].gObject.transform.position - player.position).magnitude > maxDistance + 5)
                        continue;

                    float size = 1 - ((c.worldDetail[i].gObject.transform.position - player.position).magnitude / maxDistance);

                    c.worldDetail[i].gObject.GetComponent<Transform>().localScale = new Vector3(size, size, size);

                    //if (c.worldDetail[i].gObject.transform.localScale.x <= 0 && c.worldDetail[i].gObject.activeSelf)
                    //c.worldDetail[i].gObject.SetActive(false);
                    //else 
                    if (c.worldDetail[i].gObject.transform.localScale.x > 0 && !c.worldDetail[i].gObject.activeSelf)
                        c.worldDetail[i].gObject.SetActive(true);

                    done++;
                    if (done >= (Settings.Instance.WorldDetailSet == WorldDetail.High ? 800 : 500))
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
            }
            callbackOnFinish(true);
        }
        else
        {
            callbackOnFinish(false);
        }
    }

    public Item findItem(Transform t)
    {
        foreach(Item i in items)
        {
            if (i.gObject.transform == t)
                return i;
        }
        return null;
    }
}
