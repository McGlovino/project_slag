﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkActive : MonoBehaviour
{
    public GameObject mapManager;
    MapGenerate mg;
    List<List<Chunk>> grid = new List<List<Chunk>>();
    public Chunk currentChunk;
    Chunk previousChunk;
    int distance = 3;
    bool toggleChunksRunning = false;

    public List<Chunk> radiusChunks;
    public List<Chunk> furtherChunks;



    // public List<Chunk> activeChunks = new List<Chunk>();

    int counter = 0;

    Transform player;
    Vector3 playerLastPos;

    #region Singleton
    public static ChunkActive Instance;
    private void Awake()
    {
        Instance = this;
    }
    #endregion

    void Start()
    {
        mg = mapManager.GetComponent<MapGenerate>();
        grid = mg.grid;

        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerLastPos = player.position;

        if (currentChunk == null)
            setNullChunk();

    }

    void Update()
    {
        if(GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            if (currentChunk == null)
                setNullChunk();
            else
            {
                if (counter % 10 == 0)
                {
                    if (playerLastPos != player.position)
                    {
                        currentChunk = GetChunk(transform.position);
                        if (currentChunk != previousChunk)
                        {
                            SetShouldBeActive(previousChunk, currentChunk, distance);
                            //StartCoroutine(ToggleChunks(previousChunk, currentChunk, 0.5f, distance));
                            previousChunk = currentChunk;

                            setRadiusFurtherChunks();
                        }
                        if (!toggleChunksRunning)
                        {
                            StartCoroutine(ToggleChunks2(0.25f));
                        }
                    }
                    playerLastPos = player.position;
                    counter = 0;
                }

                counter++;
            }
        }
    }

    void setNullChunk()
	{
        currentChunk = GetChunk(transform.position);
        previousChunk = currentChunk;
        if (currentChunk != null)
        {
            ToggleAll(currentChunk, distance);

            setRadiusFurtherChunks();
        }
    }

    void setRadiusFurtherChunks()
	{
        radiusChunks = this.getRadius();
        furtherChunks = this.getFurther();
    }

    public Chunk GetChunk(Vector3 from)
    {
        RaycastHit hit;
        //Physics.Raycast(from + new Vector3(0, 0.2f, 0), Vector3.down, out hit, 30f);
        if (Physics.Raycast(from + new Vector3(0, 0.3f, 0), Vector3.down, out hit, 100f, LayerMask.GetMask("Floor", "Water")))
            return FindChunk(hit.transform.parent.parent.gameObject);
        else
            return null;
    }

    public Chunk FindChunk(GameObject compareObject)
    {
        if (currentChunk != null)
        {
            if (compareObject == currentChunk.gObject)
                return currentChunk;
            for (int i = 0; i < Chunk.directions.Count; i++)
            {
                if (grid[currentChunk.x + (int)Chunk.directions[i].x][currentChunk.y + (int)Chunk.directions[i].y].gObject == compareObject)
                    return grid[currentChunk.x + (int)Chunk.directions[i].x][currentChunk.y + (int)Chunk.directions[i].y];
            }
        }
        for (int i = 0; i < grid.Count; i++) {
            for (int j = 0; j < grid[i].Count; j++)
            {
                if (grid[i][j].gObject == compareObject)
                    return grid[i][j];
            }
        }
        return null;
    }

    public void SetShouldBeActive(Chunk previous, Chunk current, int distance)
    {
        //Fix world holes when nearby
        /*foreach (Chunk c in getRadius())
        {
            c.gObject.SetActive(true);
            c.active = true;
        }*/

        Vector2 direction = Chunk.CompareChunks(previous, current);
        for (int i = 0; i < distance + 1; i++)
        {
            if ((int)direction.x != 0)
            {
                grid[current.x + ((int)direction.x * distance)][current.y + i].shouldBeActive = true;

                grid[previous.x - ((int)direction.x * distance)][current.y + i].shouldBeActive = false;

                grid[current.x + ((int)direction.x * distance)][current.y - i].shouldBeActive = true;

                grid[previous.x - ((int)direction.x * distance)][current.y - i].shouldBeActive = false;
            }
            if ((int)direction.y != 0)
            {
                grid[current.x + i][current.y + ((int)direction.y * distance)].shouldBeActive = true;

                grid[current.x + i][previous.y - ((int)direction.y * distance)].shouldBeActive = false;

                grid[current.x - i][current.y + ((int)direction.y * distance)].shouldBeActive = true;

                grid[current.x - i][previous.y - ((int)direction.y * distance)].shouldBeActive = false;
            }
            //yield return new WaitForSeconds(waitTime);
        }
    }

    IEnumerator ToggleChunks2(float waitTime)
    {
        toggleChunksRunning = true;
        foreach(List<Chunk> list in grid)
		{
            foreach (Chunk chunk in list)
            {
				if (chunk.shouldBeActive && !chunk.active)
				{
                    chunk.gObject.SetActive(true);
                    chunk.active = true;
                    yield return new WaitForEndOfFrame();
                    //yield return new WaitForEndOfFrame();
                    //yield return new WaitForEndOfFrame();
                }
                else if (!chunk.shouldBeActive && chunk.active) {
                    chunk.gObject.SetActive(false);
                    chunk.active = false;
                    yield return new WaitForEndOfFrame();
                    //yield return new WaitForEndOfFrame();
                    //yield return new WaitForEndOfFrame();
                }
            }
        }
        toggleChunksRunning = false;
    }

    IEnumerator ToggleChunks(Chunk previous, Chunk current, float waitTime, int distance)
    {
        //Fix world holes when nearby
        foreach (Chunk c in radiusChunks)
        {
            c.gObject.SetActive(true);
            c.active = true;
        }

        Vector2 direction = Chunk.CompareChunks(previous, current);
        for (int i = 0; i < distance + 1; i++)
        {
            if ((int)direction.x != 0)
            {
                Chunk temp = grid[current.x + ((int)direction.x * distance)][current.y + i];
                if (temp.shouldBeActive && !temp.active) {
                    temp.gObject.SetActive(true);
                    temp.active = true;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[previous.x - ((int)direction.x * distance)][current.y + i];
                if (!temp.shouldBeActive && temp.active)
                {
                    temp.gObject.SetActive(false);
                    temp.active = false;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[current.x + ((int)direction.x * distance)][current.y - i];
                if (temp.shouldBeActive && !temp.active)
                {
                    temp.gObject.SetActive(true);
                    temp.active = true;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[previous.x - ((int)direction.x * distance)][current.y - i];
                if (!temp.shouldBeActive && temp.active)
                {
                    temp.gObject.SetActive(false);
                    temp.active = false;
                    yield return new WaitForSeconds(waitTime);
                }
            }
            if ((int)direction.y != 0)
            {
                Chunk temp = grid[current.x + i][current.y + ((int)direction.y * distance)];
                if (temp.shouldBeActive && !temp.active)
                {
                    temp.gObject.SetActive(true);
                    temp.active = true;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[current.x + i][previous.y - ((int)direction.y * distance)];
                if (!temp.shouldBeActive && temp.active)
                {
                    temp.gObject.SetActive(false);
                    temp.active = false;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[current.x - i][current.y + ((int)direction.y * distance)];
                if (temp.shouldBeActive && !temp.active)
                {
                    temp.gObject.SetActive(true);
                    temp.active = true;
                    yield return new WaitForSeconds(waitTime);
                }
                temp = grid[current.x - i][previous.y - ((int)direction.y * distance)];
                if (!temp.shouldBeActive && temp.active)
                {
                    temp.gObject.SetActive(false);
                    temp.active = false;
                    yield return new WaitForSeconds(waitTime);
                }
            }
            //yield return new WaitForSeconds(waitTime);
        }
    }

    void ToggleAll(Chunk current, int recursion)
    {
        for (int i = 0; i < Chunk.directions.Count; i++)
        {
            Chunk compare = grid[current.x + (int)Chunk.directions[i].x][current.y + (int)Chunk.directions[i].y];
            firstRun(compare.gObject);
            compare.gObject.SetActive(true);
            grid[compare.x][compare.y].active = true;
            grid[compare.x][compare.y].shouldBeActive = true;
            //activeChunks.Add(compare);
            if (recursion > 1)
                ToggleAll(compare, recursion - 1);
        }
    }

    void firstRun(GameObject g)
    {
        if (g.GetComponentInChildren<RockyGenerate>() != null)
            g.GetComponentInChildren<RockyGenerate>().firstRun = true;
        else if (g.GetComponentInChildren<GrassyGenerate>() != null)
            g.GetComponentInChildren<GrassyGenerate>().firstRun = true;
        else if (g.GetComponentInChildren<ForestGenerate>() != null)
            g.GetComponentInChildren<ForestGenerate>().firstRun = true;
        else if (g.GetComponentInChildren<DesertGenerate>() != null)
            g.GetComponentInChildren<DesertGenerate>().firstRun = true;
        else if (g.GetComponentInChildren<SwampGenerate>() != null)
            g.GetComponentInChildren<SwampGenerate>().firstRun = true;
    }

    public List<Chunk> getRadius(Chunk check = null)
    {
        if (check == null)
            check = currentChunk;
        if (check == null)
            return null;
        List<Chunk> temp = new List<Chunk>();
        temp.Add(check);
        for (int i = 0; i < Chunk.directions.Count; i++)
        {
            temp.Add(grid[check.x + (int)Chunk.directions[i].x][check.y + (int)Chunk.directions[i].y]);
        }
        return temp;
    }

    public List<Chunk> getFurther()
    {
        List<Chunk> temp = new List<Chunk>();
        for (int i = 0; i < 2 + 1; i++)
        {
            temp.Add(grid[currentChunk.x + 2][currentChunk.y + i]);
            temp.Add(grid[currentChunk.x - 2][currentChunk.y + i]);
            temp.Add(grid[currentChunk.x + 2][currentChunk.y - i]);
            temp.Add(grid[currentChunk.x - 2][currentChunk.y - i]);

            temp.Add(grid[currentChunk.x + i][currentChunk.y + 2]);
            temp.Add(grid[currentChunk.x + i][currentChunk.y - 2]);
            temp.Add(grid[currentChunk.x - i][currentChunk.y + 2]);
            temp.Add(grid[currentChunk.x - i][currentChunk.y - 2]);

        }
        for (int i = 0; i < 3 + 1; i++)
        {
            temp.Add(grid[currentChunk.x + 3][currentChunk.y + i]);
            temp.Add(grid[currentChunk.x - 3][currentChunk.y + i]);
            temp.Add(grid[currentChunk.x + 3][currentChunk.y - i]);
            temp.Add(grid[currentChunk.x - 3][currentChunk.y - i]);

            temp.Add(grid[currentChunk.x + 3][currentChunk.y + 3]);
            temp.Add(grid[currentChunk.x + 3][currentChunk.y - 3]);
            temp.Add(grid[currentChunk.x - 3][currentChunk.y + 3]);
            temp.Add(grid[currentChunk.x - 3][currentChunk.y - 3]);

        }
        return temp;
    }
}
