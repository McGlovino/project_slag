﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class RockyGenerate : MonoBehaviour
{
    Chunk chunk;

    Material grey, darkGrey;

    //moss
    float[] m_Sparseness = { 25, 18, 16 };//ITS IN REVERSE OK T_T density
    float[] m_PatchSmallness = { 3.5f, 2.5f, 2.2f };//ITS IN REVERSE OK T_T patch size
    float[] m_EdgeBlur = { 3f, 3f, 3f };
    float[] m_Gap = { 0.75f, 0.65f, 0.62f };
    int[] m_MinSpawn = { 2, 3, 3 };
    int[] m_MaxSpawn = { 4, 5, 6 };

    //rocks
    float r_Sparseness = 80;//ITS IN REVERSE OK T_T density
    float r_PatchSmallness = 2f;//ITS IN REVERSE OK T_T patch size
    float r_EdgeBlur = 7f;
    float r_Gap = 0.74f;
    int r_MinSpawn = 0;
    int r_MaxSpawn = 2;

    //big rocks
    float br_Sparseness = 90;//ITS IN REVERSE OK T_T density
    float br_PatchSmallness = 1f;//ITS IN REVERSE OK T_T patch size
    float br_EdgeBlur = 7f;
    float br_Gap = 0.75f;
    int br_MinSpawn = 0;
    int br_MaxSpawn = 2;

    bool spawnedR, spawnedBR, spawnedWD = false;

    public bool firstRun = false;

    bool needs_to_regenerate_WD = false;
    bool needs_to_regenerate_WD_Live = false;
    WorldDetail last_WD = WorldDetail.High;

    private void Awake()
    {
        Settings.Instance.OnWorldDetailChanged += OnWorldDetailChanged;
    }

    void Start()
    {
        grey = (Material)Resources.Load("Materials/Dark Grey");
        darkGrey = (Material)Resources.Load("Materials/Dark Grey 2");

        chunk = ChunkActive.Instance.FindChunk(transform.parent.gameObject);

        float size = transform.localScale.x * transform.localScale.z;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer != 10)
                continue;

            Spawn(transform.GetChild(i));
        }
    }

    private void OnEnable()
    {
        if (last_WD != Settings.Instance.WorldDetailSet)
        {
            last_WD = Settings.Instance.WorldDetailSet;
            regenerateWorldDetail(true);
        }
        StartCoroutine(Respawn());
    }

    private void OnDisable()
    {
        ReturnToQueue();
    }
    private void Update()
    {
        if (needs_to_regenerate_WD_Live && GameStateManager.Instance.CurrentGameState != GameState.Paused)
        {
            spawnedWD = false;
            regenerateWorldDetail(false);
            needs_to_regenerate_WD_Live = false;
        }
    }

    private void OnWorldDetailChanged(WorldDetail newWorldDetail)
    {
        needs_to_regenerate_WD_Live = true;
    }

    public void regenerateWorldDetail(bool fromEnable)
    {
        if (!fromEnable)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.layer != 10)
                    continue;
                generateWorldDetail(transform.GetChild(i), true);
            }
        }
        else
            needs_to_regenerate_WD = true;

    }

    IEnumerator Respawn()
    {
        if (spawnedR && spawnedBR)
        {
            spawnedR = false; spawnedBR = false;
            int done = 0;
            for (int i = 0; i < chunk.objects.Count; i++)
            {
                if (chunk.objects[i] != null)
                {
                    chunk.objects[i].gObject = ObjectPooler.Instance.GetFromQueue(chunk.objects[i].oName, chunk.objects[i].spawnPos, Quaternion.identity, transform.parent);
                    chunk.objects[i].gObject.SetActive(true);
                }

                done++;
                if (done >= 20)
                {
                    yield return new WaitForEndOfFrame();
                    done = 0;
                }
            }
            spawnedR = true; spawnedBR = true;
        }
        if (spawnedWD)
        {
            spawnedWD = false;
            if (needs_to_regenerate_WD)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).gameObject.layer != 10)
                        continue;
                    generateWorldDetail(transform.GetChild(i), true);
                }
                needs_to_regenerate_WD = false;
            }
            else
            {
                int done = 0;
                foreach (Item i in chunk.worldDetail)
                {
                    i.gObject = ObjectPooler.Instance.GetFromQueue(i.oName, i.spawnPos, Quaternion.identity, transform.parent);
                    i.gObject.SetActive(true);

                    done++;
                    if (done >= 20)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
                spawnedWD = true;
            }
        }
    }

    void ReturnToQueue(bool justWD = false)
    {
        if (spawnedR && spawnedBR && !justWD)
        {
            foreach (Item i in chunk.objects)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
        if (spawnedWD || justWD)
        {
            foreach (Item i in chunk.worldDetail)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
    }

    void generateWorldDetail(Transform spawnOn, bool reset = false)
    {

        if (reset)
        {
            ReturnToQueue(true);
            chunk.worldDetail = new List<Item>();
        }

        int detail_level = (int)Settings.Instance.WorldDetailSet - 1;

        if (detail_level < 0)
        {
            spawnedWD = true;
            return;
        }

        //moss
        StartCoroutine(PerlinNoise.GenerateTexture2(m_PatchSmallness[detail_level], spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, m_Sparseness[detail_level], m_Gap[detail_level], firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, m_EdgeBlur[detail_level], m_MinSpawn[detail_level], m_MaxSpawn[detail_level], firstRun
                    , (returnItems) => StartCoroutine(GenerateSmallRocks(returnItems))))))));
    }
    void Spawn(Transform spawnOn)
    {
        generateWorldDetail(spawnOn);

        //small rocks
        StartCoroutine(PerlinNoise.GenerateTexture2(r_PatchSmallness, spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, r_Sparseness, r_Gap, firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, r_EdgeBlur, r_MinSpawn, r_MaxSpawn, firstRun
                    , (returnItems) => StartCoroutine(GenerateRocks(returnItems))))))));
        //big rocks
        StartCoroutine(PerlinNoise.GenerateTexture2(br_PatchSmallness, spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, br_Sparseness, br_Gap, firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, br_EdgeBlur, br_MinSpawn, br_MaxSpawn, firstRun
                    , (returnItems) => StartCoroutine(GenerateBigRocks(returnItems))))))));
    }

    IEnumerator GenerateSmallRocks(List<Vector3> returned)
    {
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.worldDetail.Add(new ITM_Moss(I, chunk));

            if (!firstRun)
                done++;
            if (done >= 15)
            {
                yield return new WaitForEndOfFrame();
                done = 0;

                if (needs_to_regenerate_WD_Live)
                    break;
            }
        }
        spawnedWD = true;
    }

    IEnumerator GenerateRocks(List<Vector3> returned)
    {
        spawnedR = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.objects.Add(new ITM_Rock(I, chunk));

            if (!firstRun)
                done++;
            if (done >= 5)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        spawnedR = true;
    }

    IEnumerator GenerateBigRocks(List<Vector3> returned)
    {
        spawnedBR = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.objects.Add(new ITM_BigRock(I, chunk));

            if (!firstRun)
                done++;
            if (done >= 5)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        spawnedBR = true;
    }
}