﻿using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwampGenerate : MonoBehaviour
{
    [HideInInspector]
    public Chunk chunk;

    //mushroom
    float minS = 15;
    float maxS = 120;
    float counter = 0;
    float timeToWait;

    //moss
    float[] m_Sparseness = { 25, 21, 17 };//ITS IN REVERSE OK T_T density
    float[] m_PatchSmallness = { 3.5f, 2.5f, 2.2f };//ITS IN REVERSE OK T_T patch size
    float[] m_EdgeBlur = { 3f, 3f, 3f };
    float[] m_Gap = { 0.75f, 0.65f, 0.6f };
    int[] m_MinSpawn = { 2, 3, 4 };
    int[] m_MaxSpawn = { 4, 6, 6 };

    bool spawned, spawnedWD = false;

    public bool firstRun = false;

    bool needs_to_regenerate_WD = false;
    bool needs_to_regenerate_WD_Live = false;
    WorldDetail last_WD = WorldDetail.High;

    private void Awake()
    {
        Settings.Instance.OnWorldDetailChanged += OnWorldDetailChanged;
    }

    void Start()
    {
        chunk = ChunkActive.Instance.FindChunk(transform.parent.gameObject);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer != 10)
                continue;

            Spawn(transform.GetChild(i));
        }
    }

    int GetMushrooms()
    {
        int temp = 0;
        foreach(Item i in chunk.objects)
        {
            if (i.oName == "Mushroom" || i.oName == "Big Mushroom")
                temp++;
        }
        return temp;
    }

    void LateUpdate()
    {
        if(this.gameObject.activeSelf == true)
        {
            if (counter >= timeToWait + (GetMushrooms() * 2)) // wait longer the more mushrooms there are
            {
                chunk.objects.Add(new ITM_Mushroom(ITM_Mushroom.GetRandomPosition(transform), chunk));
                counter = 0;
                timeToWait = Random.Range(minS, maxS);
            }
            counter += Time.deltaTime;
        }
    }

    private void OnEnable()
    {
        if (last_WD != Settings.Instance.WorldDetailSet)
        {
            last_WD = Settings.Instance.WorldDetailSet;
            regenerateWorldDetail(true);
        }
        StartCoroutine(Respawn());
    }

    private void Update()
    {
        if (needs_to_regenerate_WD_Live && GameStateManager.Instance.CurrentGameState != GameState.Paused)
        {
            spawnedWD = false;
            regenerateWorldDetail(false);
            needs_to_regenerate_WD_Live = false;
        }
    }

    private void OnWorldDetailChanged(WorldDetail newWorldDetail)
    {
        needs_to_regenerate_WD_Live = true;
    }

    public void regenerateWorldDetail(bool fromEnable)
    {
        if (!fromEnable)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.layer != 10)
                    continue;
                generateWorldDetail(transform.GetChild(i), true);
            }
        }
        else
            needs_to_regenerate_WD = true;

    }

    private void OnDisable()
    {
        ReturnToQueue();
    }

    IEnumerator Respawn()
    {
        if (spawned)
        {
            spawned = false;
            int done = 0;
            for (int i = 0; i < chunk.objects.Count; i++)
            {
                if (chunk.objects[i] != null)
                {
                    chunk.objects[i].gObject = ObjectPooler.Instance.GetFromQueue(chunk.objects[i].oName, chunk.objects[i].spawnPos, Quaternion.identity, transform.parent);
                    chunk.objects[i].gObject.SetActive(true);

                    if (chunk.objects[i].oName == "Mushroom")
                        chunk.objects[i].gObject.GetComponent<Grow>().thisItem = chunk.objects[i];
                }

                done++;
                if (done >= 20)
                {
                    yield return new WaitForEndOfFrame();
                    done = 0;
                }
            }
            spawned = true;
        }
        if (spawnedWD)
        {
            spawnedWD = false;
            if (needs_to_regenerate_WD)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).gameObject.layer != 10)
                        continue;
                    generateWorldDetail(transform.GetChild(i), true);
                }
                needs_to_regenerate_WD = false;
            }
            else
            {
                int done = 0;
                foreach (Item i in chunk.worldDetail)
                {
                    i.gObject = ObjectPooler.Instance.GetFromQueue(i.oName, i.spawnPos, Quaternion.identity, transform.parent);
                    i.gObject.SetActive(true);

                    done++;
                    if (done >= 20)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
                spawnedWD = true;
            }
        }
    }

    void ReturnToQueue(bool justWD = false)
    {
        if (spawned && !justWD)
        {
            foreach (Item i in chunk.objects)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    if (i.oName == "Mushroom")
                        i.gObject.GetComponent<Grow>().thisItem = null;
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
        if (spawnedWD || justWD)
        {
            foreach (Item i in chunk.worldDetail)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
    }

    void generateWorldDetail(Transform spawnOn, bool reset = false)
    {

        if (reset)
        {
            ReturnToQueue(true);
            chunk.worldDetail = new List<Item>();
        }

        int detail_level = (int)Settings.Instance.WorldDetailSet - 1;

        if (detail_level < 0)
        {
            spawnedWD = true;
            return;
        }

        //moss
        StartCoroutine(PerlinNoise.GenerateTexture2(m_PatchSmallness[detail_level], spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, m_Sparseness[detail_level], m_Gap[detail_level], firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, m_EdgeBlur[detail_level], m_MinSpawn[detail_level], m_MaxSpawn[detail_level], firstRun
                    , (returnItems) => StartCoroutine(GenerateMoss(returnItems))))))));
    }

    void Spawn(Transform spawnOn)
    {
        generateWorldDetail(spawnOn);

        StartCoroutine(GenerateMushroomStart(spawnOn));
    }

    IEnumerator GenerateMushroomStart(Transform spawnOn)
    {
        spawned = false;

        float size = spawnOn.localScale.x * spawnOn.localScale.z;

        //spawn some mushrooms at the start
        for (int j = 0; j < size * 3; j++)
        {
            chunk.objects.Add(new ITM_Mushroom(ITM_Mushroom.GetRandomPositionSection(spawnOn), chunk));
            if (!firstRun)
                yield return new WaitForEndOfFrame();
        }
        int bigMush = Random.Range(0, 8);
        if (bigMush == 0)
        {
            chunk.objects.Add(new ITM_BigMushroom(ITM_BigMushroom.GetRandomPositionSection(spawnOn), chunk));
        }
        spawned = true;
    }

    IEnumerator GenerateMoss(List<Vector3> returned)
    {
        spawnedWD = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.worldDetail.Add(new ITM_Moss(I, chunk, true));

            if (!firstRun)
                done++;
            if (done >= 20)
            {
                yield return new WaitForEndOfFrame();
                done = 0;

                if (needs_to_regenerate_WD_Live)
                    break;
            }
        }
        spawnedWD = true;
    }
}