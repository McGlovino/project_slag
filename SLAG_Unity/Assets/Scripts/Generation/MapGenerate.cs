﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerate : MonoBehaviour
{
    public GameObject playerCamera;

    public List<List<GameObject>> allHere = new List<List<GameObject>>();

    public Biomes addBiomes;

    int size = 100;
    int biomeSize = 4;
    int planeSize = 20;
    List<GridPos> gridDistorted = new List<GridPos>();
    public List<List<Chunk>> grid = new List<List<Chunk>>();

    [System.Serializable]
    public struct Biomes
    {
        public List<Tiles> biomes;
    }
    [System.Serializable]
    public struct Tiles
    {
        public List<Tile> tiles;
        public int amount;
    }
    [System.Serializable]
    public struct Tile
    {
        public GameObject tile;
        public int amount;
    }

    public struct GridPos
    {
        public Vector3 position;
        public List<GameObject> biomes;

        public GridPos(Vector3 vT, List<GameObject> bT)
        {
            position = vT;
            biomes = bT;
        }
    }

    void Start()
    {
        int biomelistcount = -1;
        for (int i = 0; i < addBiomes.biomes.Count; i++)
        {
            for (int j = 0; j < addBiomes.biomes[i].amount; j++)
            {
                allHere.Add(new List<GameObject>());
                biomelistcount++;
                for (int k = 0; k < addBiomes.biomes[i].tiles.Count; k++)
                {
                    for (int l = 0; l < addBiomes.biomes[i].tiles[k].amount; l++)
                    {
                        allHere[biomelistcount].Add(addBiomes.biomes[i].tiles[k].tile);
                    }
                }
            }
        }

        Generate();
    }

    void Generate()
    {
        for (int i = 0; i < size / biomeSize; i++)
        {
            for (int j = 0; j < size / biomeSize; j++)
            {
                int randomX = Random.Range(-planeSize * biomeSize / 2, planeSize * biomeSize / 2);
                int randomZ = Random.Range(-planeSize * biomeSize / 2, planeSize * biomeSize / 2);
                int randomBiome = Random.Range(0, allHere.Count);
                gridDistorted.Add(new GridPos(new Vector3(i * planeSize * biomeSize + randomX, 0, j * planeSize * biomeSize + randomZ), allHere[randomBiome]));
            }
        }

        int randomTile;
        for (int i = 0; i < size; i++)
        {
            grid.Add(new List<Chunk>());
            for (int j = 0; j < size; j++)
            {
                GridPos closest = gridDistorted[i];
                foreach (GridPos g in gridDistorted)
                {
                    if ((g.position - new Vector3(i * planeSize, 0, j * planeSize)).magnitude < (closest.position - new Vector3(i * planeSize, 0, j * planeSize)).magnitude)
                        closest = g;
                }
                randomTile = Random.Range(0, closest.biomes.Count);
                grid[i].Add(new Chunk(Instantiate(closest.biomes[randomTile], new Vector3(i * planeSize, 0, j * planeSize), Quaternion.identity, this.transform), i, j));
            }
        }
        grid[size / 2][size / 2].gObject.SetActive(true);
        playerCamera.transform.position = new Vector3((size * planeSize) / 2, 0, (size * planeSize) / 2);
    }
}
