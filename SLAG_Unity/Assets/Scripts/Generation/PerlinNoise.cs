﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PerlinNoise
{
    static int width = 64;//256;
    static int height = 64;//256;

    /*public static List<Item> GenerateObject2(Item item, Transform plane, float patchSmallness, float sparseness, float gap, float edgeBlur, float minSpawn, float maxSpawn)
    {
        List<Item> toReturn = new List<Item>();

        List<Vector3> positions = PerlinNoise.Generate(plane.transform, patchSmallness, sparseness, gap);

        foreach (Vector3 v in positions)
        {
            float minX = v.x - edgeBlur;
            float maxX = v.x + edgeBlur;
            float minZ = v.z - edgeBlur;
            float maxZ = v.z + edgeBlur;

            for (int i = 0; i < Random.Range(minSpawn, maxSpawn + 1); i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
                if (Functions.isGrounded(spawnPos))
                {
                    toReturn.Add(new Item(item.oName, item.type, spawnPos));
                }
            }
        }
        return toReturn;
    }*/

    /*public static IEnumerator GenerateObject(Item item, List<Vector3> positions, Transform plane, float edgeBlur, float minSpawn, float maxSpawn, bool firstrun, System.Action<List<Item>> callbackOnFinish)
    {
        List<Item> toReturn = new List<Item>();
        int done = 0;
        foreach (Vector3 v in positions)
        {
            float minX = v.x - edgeBlur;
            float maxX = v.x + edgeBlur;
            float minZ = v.z - edgeBlur;
            float maxZ = v.z + edgeBlur;

            for (int i = 0; i < Random.Range(minSpawn, maxSpawn + 1); i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
                if (Functions.isGrounded(spawnPos))
                    toReturn.Add(new Item(item.oName, item.type, spawnPos, item.chunk, item.otherCollect));
            }

            if(!firstrun)
                done++;
            if (done >= 10)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        callbackOnFinish(toReturn);
    }*/

    public static IEnumerator GenerateObject(List<Vector3> positions, Transform plane, float edgeBlur, float minSpawn, float maxSpawn, bool firstrun, System.Action<List<Vector3>> callbackOnFinish)
    {
        List<Vector3> toReturn = new List<Vector3>();
        int done = 0;
        foreach (Vector3 v in positions)
        {
            float minX = v.x - edgeBlur;
            float maxX = v.x + edgeBlur;
            float minZ = v.z - edgeBlur;
            float maxZ = v.z + edgeBlur;

            for (int i = 0; i < Random.Range(minSpawn, maxSpawn + 1); i++)
            {
                Vector3 spawnPos = new Vector3(Random.Range(minX, maxX), 0, Random.Range(minZ, maxZ));
                if (Functions.isGrounded(spawnPos))
                    toReturn.Add(spawnPos);
            }

            if (!firstrun)
                done++;
            if (done >= 10)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        callbackOnFinish(toReturn);
    }

    /*public static void GenerateBiomes(int size, float scale, List<GameObject> biomes)
    {
        Texture2D texture = PerlinNoise.GenerateTexture(scale, scale);

        for (int i = 0; i <size; i++)
        {
            for(int j = 0; j < size; j++)
            {
                if (texture.GetPixel(i, j).r > 0.75f)
                    MonoBehaviour.Instantiate(biomes[2], new Vector3(i*20, 0 , j*20), Quaternion.identity);
                else if (texture.GetPixel(i, j).r > 0.5f)
                    MonoBehaviour.Instantiate(biomes[1], new Vector3(i * 20, 0, j * 20), Quaternion.identity);
                else if (texture.GetPixel(i, j).r > 0.45f)
                    MonoBehaviour.Instantiate(biomes[4], new Vector3(i * 20, 0, j * 20), Quaternion.identity);
                else if (texture.GetPixel(i, j).r > 0.2)
                    MonoBehaviour.Instantiate(biomes[0], new Vector3(i * 20, 0, j * 20), Quaternion.identity);
                else
                    MonoBehaviour.Instantiate(biomes[3], new Vector3(i * 20, 0, j * 20), Quaternion.identity);
            }
        }

    }*/

    /*static List<Vector3> Generate(Transform plane, float patchSmallness, float sparseness, float gap)
    {
        List<Vector3> toReturn = new List<Vector3>();

        Texture2D texture = GenerateTexture(patchSmallness * plane.localScale.x, patchSmallness * plane.localScale.z);
        float minX = plane.position.x - plane.localScale.x * 10 / 2;
        float minZ = plane.position.z - plane.localScale.z * 10 / 2;

        int fractionX = (int)(256 / sparseness * (plane.localScale.x));
        int fractionZ = (int)(256 / sparseness * (plane.localScale.z));

        for (int x = 1; x < fractionX; x++)
        {
            for (int z = 1; z < fractionZ; z++)
            {
                if (texture.GetPixel((int)(256 * ((float)x / (float)fractionX)), (int)(256 * ((float)z / (float)(fractionZ)))).r > gap)
                    toReturn.Add(new Vector3(minX + (((float)x / (float)fractionX) * plane.localScale.x * 10), 0, minZ + (((float)z / (float)fractionZ) * plane.localScale.z * 10)));
            }
        }
        return toReturn;
    }*/

    public static IEnumerator GeneratePoints(Transform plane, Texture2D texture, float sparseness, float gap, bool firstrun, System.Action<List<Vector3>> callbackOnFinish)
    {
        List<Vector3> toReturn = new List<Vector3>();

        float minX = plane.position.x - plane.localScale.x * 10 / 2;
        float minZ = plane.position.z - plane.localScale.z * 10 / 2;

        int fractionX = (int)(256 / sparseness * (plane.localScale.x));
        int fractionZ = (int)(256 / sparseness * (plane.localScale.z));

        int done = 0;
        for (int x = 1; x < fractionX; x++)
        {
            for (int z = 1; z < fractionZ; z++)
            {
                if (texture.GetPixel((int)(256 * ((float)x / (float)fractionX)), (int)(256 * ((float)z / (float)(fractionZ)))).r > gap)
                    toReturn.Add(new Vector3(minX + (((float)x / (float)fractionX) * plane.localScale.x * 10), 0, minZ + (((float)z / (float)fractionZ) * plane.localScale.z * 10)));
            }
            if (!firstrun)
                done++;
            if (done >= 10)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        callbackOnFinish(toReturn);
    }

    /*static Texture2D GenerateTexture(float scaleX, float scaleY)
    {
        float offsetX = Random.Range(0, 999);
        float offsetY = Random.Range(0, 999);
        Texture2D texture = new Texture2D(width, height);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color colour = CalculateColour(x, y, scaleX, scaleY, offsetY, offsetX);
                texture.SetPixel(x, y, colour);
            }
        }

        texture.Apply();
        return texture;
    }*/

    public static IEnumerator GenerateTexture2(float patchSmallness, Transform plane, bool firstrun, System.Action<Texture2D> callbackOnFinish)
    {
        float scaleX = patchSmallness * plane.localScale.x;
        float scaleY = patchSmallness* plane.localScale.y;
        float offsetX = Random.Range(0, 999);
        float offsetY = Random.Range(0, 999);
        Texture2D texture = new Texture2D(width, height);

        int done = 0;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color colour = CalculateColour(x, y, scaleX, scaleY, offsetY, offsetX);
                texture.SetPixel(x, y, colour);
            }
            if (!firstrun)
                done++;
            if (done >= 10)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }

        texture.Apply();
        callbackOnFinish(texture);
    }

    static Color CalculateColour(int x, int y,float scaleX, float scaleY, float oY, float oX)
    {
        float xCoord = (float)x / width * scaleX + oX;
        float yCoord = (float)y / height * scaleY + oY;

        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        return new Color(sample, sample, sample);
    }

}
