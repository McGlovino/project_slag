﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ForestGenerate : MonoBehaviour
{
    Chunk chunk;

    //short grass
    float[] sg_Sparseness = { 25, 16, 12 };//ITS IN REVERSE OK T_T density
    float[] sg_PatchSmallness = { 2.5f, 2.2f, 1.8f };//ITS IN REVERSE OK T_T patch size
    float[] sg_EdgeBlur = { 2.5f, 2.5f, 2.5f };
    float[] sg_Gap = { 0.75f, 0.65f, 0.62f };
    int[] sg_MinSpawn = { 1, 1, 1 };
    int[] sg_MaxSpawn = { 2, 3, 4 };

    //trees
    float t_Sparseness = 75;//ITS IN REVERSE OK T_T density
    float t_PatchSmallness = 2f;//ITS IN REVERSE OK T_T patch size
    float t_EdgeBlur = 7f;
    float t_Gap = 0.5f;
    int t_MinSpawn = 1;
    int t_MaxSpawn = 4;

    float grassFromTree_EdgeBlur = 3f;

    bool spawnedTrees, spawnedWD = false;
    public bool firstRun = false;

    bool needs_to_regenerate_WD = false;
    bool needs_to_regenerate_WD_Live = false;
    WorldDetail last_WD = WorldDetail.High;

    private void Awake()
    {
        Settings.Instance.OnWorldDetailChanged += OnWorldDetailChanged;
    }

    void Start()
    {
        chunk = ChunkActive.Instance.FindChunk(transform.parent.gameObject);

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.layer != 10)
                continue;

            Spawn(transform.GetChild(i));
        }
    }

    private void OnEnable()
    {
        if (last_WD != Settings.Instance.WorldDetailSet)
        {
            last_WD = Settings.Instance.WorldDetailSet;
            regenerateWorldDetail(true);
        }
        StartCoroutine(Respawn());
    }

    private void OnDisable()
    {
        ReturnToQueue();
    }

    private void Update()
    {
        if (needs_to_regenerate_WD_Live && GameStateManager.Instance.CurrentGameState != GameState.Paused)
        {
            spawnedWD = false;
            regenerateWorldDetail(false);
            needs_to_regenerate_WD_Live = false;
        }
    }

    private void OnWorldDetailChanged(WorldDetail newWorldDetail)
    {
        needs_to_regenerate_WD_Live = true;
    }

    public void regenerateWorldDetail(bool fromEnable)
    {
        if (!fromEnable)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                if (transform.GetChild(i).gameObject.layer != 10)
                    continue;
                generateWorldDetail(transform.GetChild(i), true);
            }
        }
        else
            needs_to_regenerate_WD = true;

    }

    IEnumerator Respawn()
    {
        if (spawnedTrees)
        {
            spawnedTrees = false;
            int done = 0;
            for (int i = 0; i < chunk.objects.Count; i++)
            {
                if (chunk.objects[i] != null)
                {
                    chunk.objects[i].gObject = ObjectPooler.Instance.GetFromQueue(chunk.objects[i].oName, chunk.objects[i].spawnPos, Quaternion.identity, transform.parent);
                    chunk.objects[i].gObject.SetActive(true);
                }

                done++;
                if (done >= 75)
                {
                    yield return new WaitForEndOfFrame();
                    done = 0;
                }
            }
            spawnedTrees = true;
        }
        if (spawnedWD)
        {
            spawnedWD = false;
            if (needs_to_regenerate_WD)
            {
                for (int i = 0; i < transform.childCount; i++)
                {
                    if (transform.GetChild(i).gameObject.layer != 10)
                        continue;
                    generateWorldDetail(transform.GetChild(i), true);
                }
                needs_to_regenerate_WD = false;
            }
            else
            {
                int done = 0;
                foreach (Item i in chunk.worldDetail)
                {
                    i.gObject = ObjectPooler.Instance.GetFromQueue(i.oName, i.spawnPos, Quaternion.identity, transform.parent);
                    i.gObject.SetActive(true);

                    done++;
                    if (done >= 20)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
                spawnedWD = true;
            }
        }
    }

    void ReturnToQueue(bool justWD = false)
    {
        if (spawnedTrees && !justWD)
        {
            foreach (Item i in chunk.objects)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
        if (spawnedWD || justWD)
        {
            foreach (Item i in chunk.worldDetail)
            {
                if (i.gObject != null)
                {
                    ObjectPooler.Instance.poolDictionary[i.oName].Enqueue(i.gObject);
                    i.gObject.transform.position = new Vector3(100000, 100000, 100000);
                    i.gObject.SetActive(false);
                    i.gObject = null;
                }
            }
        }
    }

    void generateWorldDetail(Transform spawnOn, bool reset = false)
    {

        if (reset)
        {
            ReturnToQueue(true);
            chunk.worldDetail = new List<Item>();
        }

        int detail_level = (int)Settings.Instance.WorldDetailSet - 1;

        if (detail_level < 0)
        {
            spawnedWD = true;
            return;
        }

        //Short Grass
        StartCoroutine(PerlinNoise.GenerateTexture2(sg_PatchSmallness[detail_level], spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, sg_Sparseness[detail_level], sg_Gap[detail_level], firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, sg_EdgeBlur[detail_level], sg_MinSpawn[detail_level], sg_MaxSpawn[detail_level], firstRun
                    , (returnItems) => StartCoroutine(GenerateShortGrass(returnItems))))))));
    }

    void Spawn(Transform spawnOn)
    {
        generateWorldDetail(spawnOn);

        //Trees
        StartCoroutine(PerlinNoise.GenerateTexture2(t_PatchSmallness, spawnOn, firstRun
            , (returnTexture) => StartCoroutine(
                PerlinNoise.GeneratePoints(spawnOn, returnTexture, t_Sparseness, t_Gap, firstRun
                , (returnPoints) => StartCoroutine(
                    PerlinNoise.GenerateObject(returnPoints, spawnOn, t_EdgeBlur, t_MinSpawn, t_MaxSpawn, firstRun
                    , (returnItems) => StartCoroutine(GenerateTrees(returnItems))))))));
    }
    IEnumerator GenerateShortGrass(List<Vector3> returned)
    {
        spawnedWD = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.worldDetail.Add(new ITM_SmallGrass(I, chunk));

            if (!firstRun)
                done++;
            if (done >= 15)
            {
                yield return new WaitForEndOfFrame();
                done = 0;

                if (needs_to_regenerate_WD_Live)
                    break;
            }
        }
        spawnedWD = true;
    }

    IEnumerator GenerateTrees(List<Vector3> returned)
    {
        spawnedTrees = false;
        int done = 0;
        foreach (Vector3 I in returned)
        {
            chunk.objects.Add(new ITM_EvergreenTree(I, chunk));

            for (int j = 0; j < Random.Range(0, 2); j++)
            {
                Vector3 grassSpawnPos = ITM_Grass.GenerateLocation(I, grassFromTree_EdgeBlur);
                if (Functions.isGrounded(grassSpawnPos))
                    chunk.objects.Add(new ITM_Grass(grassSpawnPos, chunk));
            }
            if (!firstRun)
                done++;
            if (done >= 2)
            {
                yield return new WaitForEndOfFrame();
                done = 0;
            }
        }
        spawnedTrees = true;
    }
}