﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    private Vector3 offset;

    [Range(0.01f, 1.0f)]
    public float smooth = 2f;

    public bool lookAtPlayer = true;
    public bool rotateAroundPlayer = true;
    float rotationSpeed = 2.5f;
    float axis;

    public Transform cameraObj;
    public float minOffset;
    public float maxOffset;
    public float offsetScroll = 9.5f;
    float damping = 6f;
    float scrollSpeed = 140f;

    ClickControl clickControl;

    void Start()
    {
        offset = transform.position - target.transform.position;
        offsetScroll = (transform.position - target.position).magnitude;

        clickControl = GameObject.FindGameObjectWithTag("Player").GetComponent<ClickControl>();
    }


    void Update()
    {
        if (GameStateManager.Instance.CurrentGameState == GameState.Paused)
            return;

        //rotate
        /*if (Input.GetKey(KeyCode.Q))
            axis = -1;
        else if (Input.GetKey(KeyCode.E))
            axis = 1;
        else
            axis = 0;*/

        axis = Input.GetAxisRaw("Mouse X");

        if (rotateAroundPlayer)
        {
            Quaternion camTurnAngle = Quaternion.AngleAxis(axis * rotationSpeed, Vector3.up);
            offset = camTurnAngle * offset;
        }

        Vector3 pos = target.transform.position + offset;

        transform.position = Vector3.Slerp(transform.position, pos, smooth);

        if (lookAtPlayer)
        {
            transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z));
        }

        //scroll
        //offsetScroll = Mathf.Clamp(offsetScroll -= Input.GetAxis("Mouse ScrollWheel") * scrollSpeed * Time.deltaTime, minOffset, maxOffset);

        Vector3 dir = cameraObj.position - target.position;

        if (dir.magnitude < offsetScroll - 0.1f)
        {
            cameraObj.Translate(-Vector3.forward * (offsetScroll- dir.magnitude) * Time.deltaTime * 2);
        }
        else if (dir.magnitude > offsetScroll + 0.1f)
        {
            cameraObj.Translate(Vector3.forward * (dir.magnitude- offsetScroll)*Time.deltaTime * 2);
        }

    }
}
