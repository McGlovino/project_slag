﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    //AK
    float footStepLength = 0.425f; //actual length: 0.625f
    float footstepTimeCounter = 0;


    public Vector3 moveDir = new Vector3();
    [HideInInspector]
    public Vector3 jump = new Vector3();

    public float speed;
    float stopDistance = 0.2f;
    public Transform playerObject;

    float jumpSpeed = 10f;
    [HideInInspector]
    public float jumpHeight = 1f;
    [HideInInspector]
    public bool goingDown = false;
    bool bigJump = false;
    [HideInInspector]
    public bool jumping = false;
    [HideInInspector]
    public bool bouncing = false;

    [HideInInspector]
    public bool paused = false;

    private void Start()
    {
    }

    void Update()
    {
		if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            if (!bouncing)
            {
                float moveHorizontal = (Input.GetKey(KeyCode.A) ? -1 : 0) + (Input.GetKey(KeyCode.D) ? 1 : 0);
                float moveVertical = (Input.GetKey(KeyCode.S) ? -1 : 0) + (Input.GetKey(KeyCode.W) ? 1 : 0);

                if (Functions.isGrounded(this.transform, LayerMask.GetMask("Floor")))
                {
                    transform.position = (new Vector3(transform.position.x, 0, transform.position.z));

                    moveDir = (playerObject.right * moveHorizontal) + (playerObject.forward * moveVertical);

                    jump.y = 0;
                    goingDown = false;
                    bigJump = false;
                    jumping = false;

                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        if (Input.GetKey(KeyCode.LeftShift))
                            bigJump = true;
                        Jump();
                        jumping = true;
                        AkSoundEngine.PostEvent("Play_Jump_Voice", gameObject);
                    }
                }
                else if (transform.position.y < (bigJump ? jumpHeight * 3.5f : jumpHeight) && jumping && !goingDown)
                    Jump();
                else
                {
                    goingDown = true;
                    Jump(false);
                    //jump.y -= (jumpSpeed * (bigJump ? jumpHeight * 2 : jumpHeight)) * Time.deltaTime;
                    if (jump.y > 0)
                        Jump(false, 1.5f);
                    //jump.y -= ((jumpSpeed * 1.5f) * (bigJump ? jumpHeight * 2 : jumpHeight)) * Time.deltaTime;
                }
                //jump.y -= jumpSpeed / (30 - (10 * jumpHeight)); // DONT TOUCH

                walk(moveDir);
            }

            this.transform.position += jump * Time.deltaTime;
        }



        if (transform.position.y < 0)
            transform.position = (new Vector3(transform.position.x, 0, transform.position.z));
    }

    public void Jump(bool positive = true, float speedMultiplier = 1)
    {
        if(positive)
            jump.y += ((jumpSpeed * speedMultiplier) * (bigJump ? jumpHeight * 1.5f : jumpHeight)) * Time.deltaTime;
        else
            jump.y -= ((jumpSpeed * speedMultiplier) * (bigJump ? jumpHeight * 1f : jumpHeight)) * Time.deltaTime;

        if(moveDir == new Vector3())
            moveDir = -(playerObject.right * 0) - (playerObject.up * 0.05f);
    }

    public bool walk(Vector3 dir)
    {
        Vector3 leftPoint = playerObject.position + (-playerObject.right * 0.25f);
        Vector3 rightPoint = playerObject.position + (playerObject.right * 0.25f);
        Vector3 leftPointLow = playerObject.position + (-playerObject.right * 0.25f) - new Vector3(0,0.5f,0);
        Vector3 rightPointLow = playerObject.position + (playerObject.right * 0.25f) - new Vector3(0, 0.5f, 0);
        

        RaycastHit hit;

        if ((!Physics.Raycast(leftPoint, dir, out hit, stopDistance) || hit.transform.gameObject.tag != "NoWalk")
        && (!Physics.Raycast(rightPoint, dir, out hit, stopDistance) || hit.transform.gameObject.tag != "NoWalk")
        && (!Physics.Raycast(leftPointLow, dir, out hit, stopDistance) || hit.transform.gameObject.tag != "NoWalk")
        && (!Physics.Raycast(rightPointLow, dir, out hit, stopDistance) || hit.transform.gameObject.tag != "NoWalk")
        && (!Physics.Raycast(playerObject.position, dir, out hit, stopDistance) || hit.transform.gameObject.tag != "NoWalk"))
        {
            this.transform.position += (dir.magnitude > 1 ? dir.normalized : dir) * (float)(Input.GetKey(KeyCode.LeftShift) ? speed * 2 : speed) * Time.deltaTime;

            if (!bouncing)
            {
                footstepTimeCounter += Time.deltaTime;
                if (Functions.isGrounded(this.transform) && dir.normalized.magnitude > 0 && footstepTimeCounter > footStepLength)
                {
                    AkSoundEngine.PostEvent("Play_Default_Footsteps", gameObject);
                    footstepTimeCounter = 0;
                }
            }
            return true;
        }
        else
            return false;
    }
}