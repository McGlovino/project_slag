﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera2 : MonoBehaviour
{
    int counter = 0;
    List<Chunk> closest = new List<Chunk>();
    List<Chunk> further = new List<Chunk>();
    GameObject player;
    Transform playerObj;

    Camera camera;

    bool lookAgain = true;
    bool lookFurtherAgain = true;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerObj = player.transform.GetChild(0);
        camera = this.transform.GetComponent<Camera>();
    }

    void LateUpdate()
    {
		if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            playerObj.LookAt(new Vector3(transform.position.x, playerObj.position.y, transform.position.z));
            //player.transform.GetChild(0).eulerAngles = new Vector3(player.transform.GetChild(0).eulerAngles.x + 90, player.transform.GetChild(0).eulerAngles.y, player.transform.eulerAngles.z);
            playerObj.eulerAngles = new Vector3(playerObj.eulerAngles.x, playerObj.eulerAngles.y + 180, player.transform.eulerAngles.z);

            if (lookAgain) LookCall();
            if (lookFurtherAgain) LookFurtherCall();

            if (counter % 10 == 0)
                closest = ChunkActive.Instance.radiusChunks;
            if (counter % 20 == 0)
            {
                further = ChunkActive.Instance.furtherChunks;
                counter = 0;
            }
            counter++;
        }
    }

    void LookCall()
    {
        lookAgain = false;
        StartCoroutine(Look(closest, (again) => lookAgain = again));
    }

    IEnumerator Look(List<Chunk> closestT, System.Action<bool> callbackOnFinish)
    {
        int done = 0;
        foreach (Chunk c in closestT)
        {
            for (int i = 0; i < c.objects.Count; i++)// Item I in c.objects)
            {
                Vector3 temp = camera.WorldToViewportPoint(c.objects[i].spawnPos);
                if (Mathf.Clamp(temp.x, -0.2f, 1.2f) == temp.x && Mathf.Clamp(temp.y, -0.2f, 1.2f) == temp.y && temp.z >= 0) {
                    if (c.objects[i].gObject != null)
                        c.objects[i].gObject.transform.LookAt(new Vector3(transform.position.x, c.objects[i].gObject.transform.position.y, transform.position.z));
                    else
                        Debug.Log("Null Look Object: " + c.objects[i].oName);

                    done++;
                    if (done >= 300)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
            }
        }
        callbackOnFinish(true);
    }

    public void LookObject(GameObject g)
    {
        g.transform.LookAt(new Vector3(transform.position.x, g.transform.position.y, transform.position.z));
    }

    void LookFurtherCall()
    {
        lookFurtherAgain = false;
        StartCoroutine(LookFurther(further, (again) => lookFurtherAgain = again));
    }

    IEnumerator LookFurther(List<Chunk> furtherT, System.Action<bool> callbackOnFinish)
    {
        int done = 0;
        foreach (Chunk c2 in furtherT)
        {
            for (int i = 0; i < c2.objects.Count; i++)// Item I in c.objects)
            {
                Vector3 temp = camera.WorldToViewportPoint(c2.objects[i].spawnPos);
                if (Mathf.Clamp(temp.x, -0.2f, 1.2f) == temp.x && Mathf.Clamp(temp.y, -0.2f, 1.2f) == temp.y && temp.z >= 0)
                {
                    if (c2.objects[i].gObject != null)
                        c2.objects[i].gObject.transform.LookAt(new Vector3(transform.position.x, c2.objects[i].gObject.transform.position.y, transform.position.z));

                    done++;
                    if (done >= 200)
                    {
                        yield return new WaitForEndOfFrame();
                        done = 0;
                    }
                }
            }
        }
        callbackOnFinish(true);
    }
}
