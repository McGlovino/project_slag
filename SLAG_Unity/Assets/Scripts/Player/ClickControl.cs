﻿using JetBrains.Annotations;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ClickControl : MonoBehaviour
{
    public enum slotType { tool, inventory, recipe };

    float distanceMax = 1.5f;
    Item closest;
    float counter = 0;


    public Inventory inventory = new Inventory();
    public GameObject invArrow;

    public List<GameObject> toolTexts = new List<GameObject>();
    List<Vector2> toolLocations = new List<Vector2>();
    public List<GameObject> invTexts = new List<GameObject>();
    List<Vector2> invLocations = new List<Vector2>();
    public List<GameObject> recipeTexts = new List<GameObject>();
    List<Vector2> recipeLocations = new List<Vector2>();

    int currentlySelected;
    float minWaitScroll = 0f;
    float timeSinceLastScroll;
    int ySpacing = 25;
    float fadeVal = 30;

    bool inventoryOpen = true;
    float offset = 0;
    float gapBetweenInvRecipes = 10f;

    Color OutlineColour = new Color32(0, 0, 0, 255);
    Color inventoryColour = new Color32(225, 210, 210, 255);
    Color recipeColour = new Color32(115, 193, 185, 255);
    Color toolColourStart = new Color32(130, 180, 100, 255);
    Color toolColourEnd = new Color32(210, 84, 86, 255);

    [HideInInspector]
    public bool paused = false;
    public GameObject pauseMenu;
    public List<GameObject> pauseMenuTexts = new List<GameObject>();
    int pauseCurrentlySelected = 0;
    List<Vector2> pauseLocations = new List<Vector2>();
    public List<GameObject> settingsTexts = new List<GameObject>();
    int settingsCurrentlySelected = 0;
    List<Vector2> settingsLocations = new List<Vector2>();
    int pauseMenuState = 0; //0 main, 1 settings


    public List<Text> worldDetailOptions = new List<Text>();
    public List<Text> shadowOptions = new List<Text>();

    void Start()
    {
        Cursor.visible = false;
        PrintInventory();
        //invText.gameObject.SetActive(inventoryOpen);

        ForceInvLocations(true);
        ForcePauseLocations(true);
        ForceSettingsLocations(true);
        setShadowMarked();
        setWorldDetailMarked();
    }

    void Update()
    {
        TriggerPause();

        if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
        {
            DoInput();
            MoveInv();
            ItemUpdate();

            if (counter % 4 == 0)
            {
                Select();
                counter = 0;
            }
            counter++;
        }
		else
		{
            if (pauseMenuState == 0)
                MovePause();
            else
                MoveSettings();

            DoPauseInput();
        }
    }

    void TriggerPause()
	{
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
                Pause();
            else
                Resume();
        }
    }

    void Pause()
	{
        paused = true;
        //Time.timeScale = 1;
        pauseMenu.SetActive(true);

        GameStateManager.Instance.SetState(GameState.Paused);

        pauseMenuState = 0;
        pauseCurrentlySelected = 0;
        settingsCurrentlySelected = 0;
        MovePause(true);
        MoveSettings(true);

        foreach (GameObject g in pauseMenuTexts)
            g.SetActive(true);
        foreach (GameObject g in settingsTexts)
            g.SetActive(false);

        foreach (GameObject g in toolTexts)
            g.SetActive(false);
        foreach (GameObject g in invTexts)
            g.SetActive(false);
        foreach (GameObject g in recipeTexts)
            g.SetActive(false);
        invArrow.SetActive(false);
    }

	void Resume()
	{
        paused = false;
        //Time.timeScale = 0;
        pauseMenu.SetActive(false);

        GameStateManager.Instance.SetState(GameState.Gameplay);

        foreach (GameObject g in toolTexts)
            g.SetActive(true);
        foreach (GameObject g in invTexts)
            g.SetActive(true);
        foreach (GameObject g in recipeTexts)
            g.SetActive(true);
        invArrow.SetActive(true);
	}

	void DoPauseInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			if (pauseMenuState == 0)
			{
                switch (pauseCurrentlySelected)
                {
                    case 0:
                        Resume();
                        break;
                    case 1:
                        toggleSettingsPause();
                        break;
                    case 2:
                        Application.Quit();
                        break;
                    default:
                        break;
                }
            }
            else if (pauseMenuState == 1)
			{
                switch (settingsCurrentlySelected)
                {
                    case 0:
                        Settings.Instance.ChangeWorldDetail();
                        setWorldDetailMarked();
                        break;
                    case 1:
                        Settings.Instance.ChangeShadows();
                        setShadowMarked();
                        break;
                    case 2:
                        toggleSettingsPause();
                        break;
                    default:
                        break;
                }
            }
		}
	}
    void setWorldDetailMarked()
	{
        switch (Settings.Instance.WorldDetailSet)
		{
            case WorldDetail.Off:
                worldDetailOptions[0].color = toolColourStart;
                worldDetailOptions[1].color = new Color(1, 1, 1);
                worldDetailOptions[2].color = new Color(1, 1, 1);
                worldDetailOptions[3].color = new Color(1, 1, 1);
                break;
            case WorldDetail.Low:
                worldDetailOptions[0].color = new Color(1, 1, 1);
                worldDetailOptions[1].color = toolColourStart;
                worldDetailOptions[2].color = new Color(1, 1, 1);
                worldDetailOptions[3].color = new Color(1, 1, 1);
                break;
            case WorldDetail.Mid:
                worldDetailOptions[0].color = new Color(1, 1, 1);
                worldDetailOptions[1].color = new Color(1, 1, 1);
                worldDetailOptions[2].color = toolColourStart;
                worldDetailOptions[3].color = new Color(1, 1, 1);
                break;
            case WorldDetail.High:
                worldDetailOptions[0].color = new Color(1, 1, 1);
                worldDetailOptions[1].color = new Color(1, 1, 1);
                worldDetailOptions[2].color = new Color(1, 1, 1);
                worldDetailOptions[3].color = toolColourStart;
                break;
        }
	}
    void setShadowMarked()
    {
        switch (Settings.Instance.ShadowsSet)
        {
            case Shadows.Low:
                shadowOptions[0].color = toolColourStart;
                shadowOptions[1].color = new Color(1, 1, 1);
                shadowOptions[2].color = new Color(1, 1, 1);
                break;
            case Shadows.Mid:
                shadowOptions[0].color = new Color(1, 1, 1);
                shadowOptions[1].color = toolColourStart;
                shadowOptions[2].color = new Color(1, 1, 1);
                break;
            case Shadows.High:
                shadowOptions[0].color = new Color(1, 1, 1);
                shadowOptions[1].color = new Color(1, 1, 1);
                shadowOptions[2].color = toolColourStart;
                break;
        }
    }
    void toggleSettingsPause()
    {
        pauseMenuState = pauseMenuState == 1 ? 0 : 1;
        pauseCurrentlySelected = 0;
        settingsCurrentlySelected = 0;
        MovePause(true);
        MoveSettings(true);

        foreach (GameObject g in pauseMenuTexts)
            g.SetActive(!g.activeSelf);
        foreach (GameObject g in settingsTexts)
            g.SetActive(!g.activeSelf);
    }

    public void MoveSettings(bool force = false)
    {
        bool change = false;
        if (timeSinceLastScroll >= minWaitScroll || force)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < -(1 * Time.deltaTime) && settingsCurrentlySelected < settingsTexts.Count - 1)
            {
                settingsCurrentlySelected++;
                change = true;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > (1 * Time.deltaTime) && settingsCurrentlySelected > 0)
            {
                settingsCurrentlySelected--;
                change = true;
            }
            if (change || force)
            {
                timeSinceLastScroll = 0;

                ForceSettingsLocations();
            }
        }
        if (settingsLocations[0] != settingsTexts[0].GetComponent<RectTransform>().anchoredPosition)
        {
            for (int i = 0; i < settingsTexts.Count; i++)
                settingsTexts[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(settingsTexts[i].GetComponent<RectTransform>().anchoredPosition, settingsLocations[i], 0.6f);
        }

        timeSinceLastScroll += Time.deltaTime;
    }
    void ForceSettingsLocations(bool start = false)
    {
        offset = (settingsTexts.Count == 0 ? 0 : (settingsCurrentlySelected * ySpacing) - (settingsTexts.Count % 2 == 0 ? (ySpacing / 2) : 0) - (ySpacing * Mathf.Floor((settingsTexts.Count - 1) / 2)));

        for (int i = 0; i < settingsTexts.Count; i++)
        {
            if (start)
            {
                settingsLocations.Add(new Vector2(settingsTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((settingsTexts.Count / 2) * ySpacing) - (settingsTexts.Count % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i))) + new Vector2(0, offset));
            }
            else
                settingsLocations[i] = (new Vector2(settingsTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((settingsTexts.Count / 2) * ySpacing) - (settingsTexts.Count % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i))) + new Vector2(0, offset));
        }

        if (settingsCurrentlySelected >= settingsTexts.Count)
            settingsCurrentlySelected = settingsTexts.Count - 1;
        if (settingsCurrentlySelected < 0)
            settingsCurrentlySelected = 0;
    }

    public void MovePause(bool force = false)
    {
        bool change = false;
        if (timeSinceLastScroll >= minWaitScroll || force)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < -(1 * Time.deltaTime) && pauseCurrentlySelected < pauseMenuTexts.Count - 1)
            {
                pauseCurrentlySelected++;
                change = true;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > (1 * Time.deltaTime) && pauseCurrentlySelected > 0)
            {
                pauseCurrentlySelected--;
                change = true;
            }
            if (change || force)
            {
                timeSinceLastScroll = 0;

                ForcePauseLocations();
            }
        }
        if (pauseLocations[0] != pauseMenuTexts[0].GetComponent<RectTransform>().anchoredPosition)
        {
            for (int i = 0; i < pauseMenuTexts.Count; i++)
                pauseMenuTexts[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(pauseMenuTexts[i].GetComponent<RectTransform>().anchoredPosition, pauseLocations[i], 0.6f);
        }

        timeSinceLastScroll += Time.deltaTime;
    }

    void ForcePauseLocations(bool start = false)
    {
        offset = (pauseMenuTexts.Count == 0 ? 0 : (pauseCurrentlySelected * ySpacing) - (pauseMenuTexts.Count % 2 == 0 ? (ySpacing / 2) : 0) - (ySpacing * Mathf.Floor((pauseMenuTexts.Count - 1) / 2)));

        for (int i = 0; i < pauseMenuTexts.Count; i++)
        {
            if (start)
            {
                pauseLocations.Add(new Vector2(pauseMenuTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((pauseMenuTexts.Count / 2) * ySpacing) - (pauseMenuTexts.Count % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i))) + new Vector2(0, offset));
            }
            else
                pauseLocations[i] = (new Vector2(pauseMenuTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((pauseMenuTexts.Count / 2) * ySpacing) - (pauseMenuTexts.Count % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i))) + new Vector2(0, offset));
        }

        if (pauseCurrentlySelected >= pauseMenuTexts.Count)
            pauseCurrentlySelected = pauseMenuTexts.Count - 1;
        if (pauseCurrentlySelected < 0)
            pauseCurrentlySelected = 0;
    }


    void ForceInvLocations(bool start = false)
    {
        int totalCount = (toolTexts.Count + invTexts.Count + recipeTexts.Count);
        offset = (totalCount == 0 ? 0 : (currentlySelected * ySpacing) - (totalCount % 2 == 0 ? (ySpacing/2) : 0) - (ySpacing * Mathf.Floor((totalCount-1) / 2)));

        slotType selectedType;
        if (currentlySelected > (toolTexts.Count + invTexts.Count) - 1)
            selectedType = slotType.recipe;
        else if (currentlySelected > toolTexts.Count - 1)
            selectedType = slotType.inventory;
        else
            selectedType = slotType.tool;

        for (int i = 0; i < toolTexts.Count; i++)
        {
            toolLocations[i] = (new Vector2(toolTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((totalCount / 2) * ySpacing) - (totalCount % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i)))) + new Vector2(0, offset);

            if (selectedType == slotType.inventory)
                toolLocations[i] += new Vector2(0, gapBetweenInvRecipes);
            if (selectedType == slotType.recipe)
                toolLocations[i] += new Vector2(0, gapBetweenInvRecipes * 2);

            toolTexts[i].GetComponent<Text>().color = Color.Lerp(toolColourStart, toolColourEnd, inventory.tools[i][0].PercentDamaged());
                //- new Color(0, 0, 0, (Mathf.Abs(toolLocations[i].y) / ySpacing) / fadeVal);

            //toolTexts[i].GetComponent<Outline>().effectColor = OutlineColour - new Color(0, 0, 0, (Mathf.Abs(invLocations[i].y) / ySpacing) / fadeVal);
        }
        for (int i = 0; i < invTexts.Count; i++)
        {
            if (start)
            {
                invLocations.Add(new Vector2(invTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((totalCount / 2) * ySpacing) - (totalCount % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i + toolTexts.Count))) + new Vector2(0, offset));
                invTexts[i].GetComponent<Text>().color = inventoryColour;
                invTexts[i].GetComponent<Outline>().effectColor = OutlineColour;
            }
            else
                invLocations[i] = (new Vector2(invTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((totalCount / 2) * ySpacing) - (totalCount % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i + toolTexts.Count)))) + new Vector2(0, offset);

            if (selectedType == slotType.tool)
                invLocations[i] -= new Vector2(0, gapBetweenInvRecipes);
            if (selectedType == slotType.recipe)
                invLocations[i] += new Vector2(0, gapBetweenInvRecipes);

            //invTexts[i].GetComponent<Text>().color = inventoryColour - new Color(0, 0, 0, (Mathf.Abs(invLocations[i].y) / ySpacing) / fadeVal);
            //invTexts[i].GetComponent<Outline>().effectColor = OutlineColour - new Color(0, 0, 0, (Mathf.Abs(invLocations[i].y) / ySpacing) / fadeVal);
        }
        for (int i = 0; i < recipeTexts.Count; i++)
        {
            recipeLocations[i] = (new Vector2(recipeTexts[i].GetComponent<RectTransform>().anchoredPosition.x
                , ((totalCount / 2) * ySpacing) - (totalCount % 2 == 0 ? ySpacing / 2 : 0) - (ySpacing * (i + toolTexts.Count + invTexts.Count)))) + new Vector2(0, offset);

            if (selectedType == slotType.tool)
                recipeLocations[i] -= new Vector2(0, gapBetweenInvRecipes * 2);
            if (selectedType == slotType.inventory)
                recipeLocations[i] -= new Vector2(0, gapBetweenInvRecipes);

            //recipeTexts[i].GetComponent<Text>().color = recipeColour - new Color(0, 0, 0, (Mathf.Abs(recipeLocations[i].y) / ySpacing) / fadeVal);
           // recipeTexts[i].GetComponent<Outline>().effectColor = OutlineColour - new Color(0, 0, 0, (Mathf.Abs(recipeLocations[i].y) / ySpacing) / fadeVal);
        }

        if (start)
            currentlySelected = ((invTexts.Count / 2) - 1 > 0 ? (invTexts.Count / 2) - 1 : 0) + toolTexts.Count;

        if (currentlySelected >= totalCount)
            currentlySelected = totalCount-1;
        if (currentlySelected < 0)
            currentlySelected = 0;

        //arrow location
        //arrow colour
        if (currentlySelected > (toolTexts.Count + invTexts.Count) - 1)
        {
            //invArrow.GetComponent<RectTransform>().anchoredPosition = recipeLocations[currentlySelected - invLocations.Count - toolLocations.Count] - new Vector2(27, 0);
            invArrow.GetComponent<Text>().color = recipeColour;
        }
        else if (currentlySelected > toolTexts.Count - 1)
        {
            //invArrow.GetComponent<RectTransform>().anchoredPosition = invLocations[currentlySelected - toolTexts.Count] - new Vector2(27, 0);
            invArrow.GetComponent<Text>().color = inventoryColour;
        }
        else
        {
            //invArrow.GetComponent<RectTransform>().anchoredPosition = toolLocations[currentlySelected] - new Vector2(27, 0);
            invArrow.GetComponent<Text>().color = toolTexts[currentlySelected].GetComponent<Text>().color;
        }
        invArrow.GetComponent<Outline>().effectColor = OutlineColour;
    }

    void ItemUpdate()
    {
        if (currentlySelected < toolTexts.Count)
            if (inventory.tools[currentlySelected][0].toolType == Item.toolTypeOf.Torch)
            {
                //inventory.tools[currentlySelected][0].Damage(Time.deltaTime);
                toolTexts[currentlySelected].GetComponent<Text>().color = Color.Lerp(toolColourStart, toolColourEnd, inventory.tools[currentlySelected][0].PercentDamaged());
                //- new Color(0, 0, 0, 0);//(Mathf.Abs(toolLocations[currentlySelected].y) / ySpacing) / fadeVal
                invArrow.GetComponent<Text>().color = toolTexts[currentlySelected].GetComponent<Text>().color;
            }
    }

    void EnableTool()
    {
        foreach (List<Item> i in inventory.tools)
        {
            if (i[0].type == Item.typeOf.tool) {
                foreach (Transform t in GetChildRecursive(i[0].gObject.transform))
                {
                    if (t.gameObject.GetComponent<MeshRenderer>() != null)
                        t.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    if (t.gameObject.GetComponent<Light>() != null)
                        t.gameObject.GetComponent<Light>().enabled = false;
                }
            }
        }
        if (currentlySelected < inventory.tools.Count && inventory.tools[currentlySelected][0].type == Item.typeOf.tool)
        {
            foreach (Transform t in GetChildRecursive(inventory.tools[currentlySelected][0].gObject.transform))
            {
                if (t.gameObject.GetComponent<MeshRenderer>() != null)
                    t.gameObject.GetComponent<MeshRenderer>().enabled = true;
                if (t.gameObject.GetComponent<Light>() != null)
                    t.gameObject.GetComponent<Light>().enabled = true;
            }
        }
    }

    void EnableTool(Item enable , bool off = true)
    {
        if (off && enable.type == Item.typeOf.tool)
        {
            foreach (Transform t in GetChildRecursive(enable.gObject.transform))
            {
                if (t.gameObject.GetComponent<MeshRenderer>() != null)
                    t.gameObject.GetComponent<MeshRenderer>().enabled = false;
                if (t.gameObject.GetComponent<Light>() != null)
                    t.gameObject.GetComponent<Light>().enabled = false;
            }
        }
        else if (enable.type == Item.typeOf.tool)
        {
            foreach (Transform t in GetChildRecursive(enable.gObject.transform))
            {
                if (t.gameObject.GetComponent<MeshRenderer>() != null)
                    t.gameObject.GetComponent<MeshRenderer>().enabled = true;
                if (t.gameObject.GetComponent<Light>() != null)
                    t.gameObject.GetComponent<Light>().enabled = true;
            }
        }
    }

    public void MoveInv(bool force = false)
    {
        bool change = false;
        if (timeSinceLastScroll >= minWaitScroll || force)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < -(1 * Time.deltaTime) && currentlySelected < (toolTexts.Count + invTexts.Count + recipeTexts.Count) - 1)
            {
                currentlySelected++;
                change = true;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") > (1 * Time.deltaTime) && currentlySelected > 0)
            {
                currentlySelected--;
                change = true;
            }
            if (change || force)
            {
                timeSinceLastScroll = 0;

                ForceInvLocations();
            }
        }
        if (invLocations[0] != invTexts[0].GetComponent<RectTransform>().anchoredPosition 
            || (recipeLocations.Count > 0 && (recipeLocations[0] != recipeTexts[0].GetComponent<RectTransform>().anchoredPosition))
            || (toolLocations.Count > 0 && (toolLocations[0] != toolTexts[0].GetComponent<RectTransform>().anchoredPosition))) {
            for (int i = 0; i < toolTexts.Count; i++)
                toolTexts[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(toolTexts[i].GetComponent<RectTransform>().anchoredPosition, toolLocations[i], 0.6f);
            for (int i = 0; i < invTexts.Count; i++)
                invTexts[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(invTexts[i].GetComponent<RectTransform>().anchoredPosition, invLocations[i], 0.6f);
            for (int i = 0; i < recipeTexts.Count; i++)
                recipeTexts[i].GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(recipeTexts[i].GetComponent<RectTransform>().anchoredPosition, recipeLocations[i], 0.6f);
        }
        EnableTool();

        timeSinceLastScroll += Time.deltaTime;
    }

    void DoInput()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventoryOpen = !inventoryOpen;
            //invText.gameObject.SetActive(inventoryOpen);
        }

        if (Input.GetKeyDown(KeyCode.P))
            ItemManager.Instance.ToggleWorldDetail();

        //left click: world interation
        if (Input.GetMouseButtonDown(0))
        {
            if (currentlySelected < inventory.tools.Count && inventory.tools[currentlySelected][0].type == Item.typeOf.tool)
                inventory.tools[currentlySelected][0].gObject.GetComponent<Animation>().Play("Use Tool");
            if (closest != null && closest.type != Item.typeOf.select)
            {
                if (closest.type == Item.typeOf.tool)
                {
                    EnableTool(closest);

                    if(inventory.AddItem(closest.PickUp
                    ((currentlySelected < inventory.tools.Count ? inventory.tools[currentlySelected][0] :
                    (currentlySelected < inventory.tools.Count + inventory.items.Count ? inventory.items[currentlySelected - inventory.tools.Count][0] : null))), inventory.tools))
                        currentlySelected++;
                }
                else 
                {
                    Item returned = closest.PickUp
                        ((currentlySelected < inventory.tools.Count ? inventory.tools[currentlySelected][0] :
                        (currentlySelected < inventory.tools.Count + inventory.items.Count ? inventory.items[currentlySelected - inventory.tools.Count][0] : null)));
                    if (returned != null)
                        inventory.AddItem(returned);
                }
                PrintInventory();
                MoveInv(true);
            }
        }

        //right click: inventory interaction
        if (Input.GetMouseButtonDown(1))
        {
            //if (currentlySelected > inventory.tools.Count - 1 && inventory.items.Count > 0)
            //{
                if (inventory.tools.Count > 0 && currentlySelected < inventory.tools.Count)
                {
                    if(inventory.tools[currentlySelected][0].toolType == Item.toolTypeOf.Torch)
                        inventory.RemoveItem(currentlySelected, inventory.tools).Place(transform, gameObject.GetComponent<ChunkActive>().currentChunk);
                }
                else if(inventory.items.Count > 0 && currentlySelected < inventory.tools.Count + inventory.items.Count)
                {
                    if(inventory.items[currentlySelected - toolTexts.Count][0].canPlace)
                        inventory.RemoveItem(currentlySelected - toolTexts.Count, inventory.items).Place(transform, gameObject.GetComponent<ChunkActive>().currentChunk);
                }
                else if(inventory.items.Count > 0 && inventory.recipes.Count > 0)
                {
                    bool reduce = (inventory.recipiesCanCraft[currentlySelected - toolTexts.Count - invTexts.Count] == 1);
                    if (!inventory.InInventory(inventory.recipes[currentlySelected - toolTexts.Count - invTexts.Count].crafting, inventory.items)
                    && !inventory.InInventory(inventory.recipes[currentlySelected - toolTexts.Count - invTexts.Count].crafting, inventory.tools))
                    {
                        inventory.CraftItem(currentlySelected);
                        currentlySelected++;
                    }
                    else
                        inventory.CraftItem(currentlySelected);
                if (reduce)
                    ForceInvLocations();
            }
                PrintInventory();
            //}
        }
    }

    public void PrintInventory()
    {
        if (inventoryOpen)
        {
            /// Recipes
            List<string> recipeStrings = inventory.CalculateRecipies();
            //make sure same length, remove deleted items
            while ((recipeStrings == null && recipeTexts.Count > 0) || (recipeStrings != null && (recipeTexts.Count > recipeStrings.Count)))
            {
                if (currentlySelected == (invTexts.Count + toolTexts.Count + recipeTexts.Count) - 1)
                    currentlySelected--;
                Destroy(recipeTexts[recipeTexts.Count - 1]);
                recipeTexts.Remove(recipeTexts[recipeTexts.Count - 1]);
                recipeLocations.Remove(recipeLocations[recipeLocations.Count - 1]);

                ForceInvLocations();
            }
            if (recipeStrings != null)
            {
                //add new slot
                while (recipeTexts.Count < recipeStrings.Count)
                    AddNewInvSlot(slotType.recipe);
                //set text
                for (int i = 0; i < recipeTexts.Count; i++)
                    recipeTexts[i].GetComponent<Text>().text = recipeStrings[i];
            }
            
            ///inventory
            List<string> strings = inventory.ListOfContents(inventory.items);
            //make sure same length, remove deleted items
            while (invTexts.Count > strings.Count)
            {
                if (currentlySelected == (invTexts.Count + toolTexts.Count) - 1)
                    currentlySelected--;
                Destroy(invTexts[invTexts.Count - 1]);
                invTexts.Remove(invTexts[invTexts.Count - 1]);
                invLocations.Remove(invLocations[invLocations.Count - 1]);

                ForceInvLocations();
            }
            //add new slot
            while (invTexts.Count < strings.Count)
                AddNewInvSlot(slotType.inventory);
            //set text
            for (int i = 0; i < invTexts.Count; i++)
                invTexts[i].GetComponent<Text>().text = strings[i];

            ///Tools
            List<string> stringsTool = inventory.ListOfContents(inventory.tools);
            //make sure same length, remove deleted items
            while ((stringsTool == null && toolTexts.Count > 0) || (stringsTool != null && (toolTexts.Count > stringsTool.Count)))
            {
                if (currentlySelected == (toolTexts.Count) - 1)
                    currentlySelected--;
                Destroy(toolTexts[toolTexts.Count - 1]);
                toolTexts.Remove(toolTexts[toolTexts.Count - 1]);
                toolLocations.Remove(toolLocations[toolLocations.Count - 1]);

                ForceInvLocations();
            }
            //add new slot
            if (stringsTool != null)
            {
                while (toolTexts.Count < stringsTool.Count)
                    AddNewInvSlot(slotType.tool);
                //set text
                for (int i = 0; i < toolTexts.Count; i++)
                    toolTexts[i].GetComponent<Text>().text = stringsTool[i];
            }
        }
    }

    void Select()
    {
        if(closest != null && closest.gObject != null)
            changeTo(closest.gObject.transform, "Default");
        closest = null;

        List<Chunk> nearby = ChunkActive.Instance.radiusChunks;
        if (nearby == null)
            return;
        //finds closest object and highlights
        foreach (Chunk c in nearby)
        {
            for (int j = 0; j < c.objects.Count; j++)
            {
                if (c.objects[j].gObject == null)
                    break;
                float distT = Vector3.Distance(c.objects[j].gObject.transform.position, transform.position);
                if (closest != null && closest.gObject != null)
                {
                    if (distT < Vector3.Distance(closest.gObject.transform.position, transform.position) && distT < distanceMax && Functions.isGrounded(c.objects[j].gObject, 0.2f))
                    {
                        changeTo(closest.gObject.transform, "Default");
                        closest = c.objects[j];
                        changeTo(closest.gObject.transform, "Outline");
                    }
                }
                else// if (closest == null)
                {
                    if (distT < distanceMax && Functions.isGrounded(c.objects[j].gObject))
                    {
                        closest = c.objects[j];
                        changeTo(closest.gObject.transform, "Outline");
                    }
                }
            }
        }

        //if moves out of distance or player is jumping then nothing highlighted
        if (closest != null)
        {
            if ((closest.gObject.transform.position - transform.position).magnitude > distanceMax || !Functions.isGrounded(this.transform))
            {
                changeTo(closest.gObject.transform, "Default");
                closest = null;
            }
        }
    }

    void AddNewInvSlot(slotType type = slotType.inventory)
    {
        if (invTexts[0].GetComponent<Text>().text != "No Items")
        {
            GameObject temp = new GameObject();

            RectTransform rect = temp.AddComponent<RectTransform>();
            RectTransform rect_copy = invTexts[0].GetComponent<RectTransform>();
            rect.anchorMin = rect_copy.anchorMin;
            rect.anchorMax = rect_copy.anchorMax;
            rect.sizeDelta = rect_copy.sizeDelta;
            rect.localScale = rect_copy.localScale;

           temp.AddComponent<CanvasRenderer>();

            Text text = temp.AddComponent<Text>();
            Text text_copy = invTexts[0].GetComponent<Text>();
            text.font = text_copy.font;
            text.fontStyle = text_copy.fontStyle;
            text.fontSize = text_copy.fontSize;

            Outline outline = temp.AddComponent<Outline>();
            outline.effectColor = OutlineColour;
            //Outline outline_copy = invTexts[0].GetComponent<Outline>();
            //outline.effectColor = outline_copy.effectColor;

            temp.transform.SetParent(invTexts[0].transform.parent, false);
            //temp.transform.parent = invTexts[0].transform.parent;
            Vector2 tempPos = new Vector2();

            switch (type)
            {
                case slotType.tool:
                    temp.GetComponent<Text>().color = toolColourStart;
                    if (toolLocations.Count > 0)
                        tempPos = toolLocations[toolLocations.Count - 1] - new Vector2(0, ySpacing);
                    else
                        tempPos = invLocations[0] + new Vector2(0, ySpacing + gapBetweenInvRecipes);
                    break;
                case (slotType.inventory):
                    temp.GetComponent<Text>().color = inventoryColour;
                    tempPos = invLocations[invLocations.Count - 1] - new Vector2(0, ySpacing);
                    break;
                case (slotType.recipe):
                    temp.GetComponent<Text>().color = recipeColour;
                    if (recipeLocations.Count > 0)
                        tempPos = recipeLocations[recipeLocations.Count - 1] - new Vector2(0, ySpacing);
                    else
                        tempPos = invLocations[invLocations.Count - 1] - new Vector2(0, ySpacing + gapBetweenInvRecipes);
                    break;
            }

            temp.GetComponent<RectTransform>().anchoredPosition = tempPos;

            switch (type)
            {
                case slotType.tool:
                    toolTexts.Add(temp);
                    toolLocations.Add(tempPos);
                    break;
                case (slotType.inventory):
                    invTexts.Add(temp);
                    invLocations.Add(tempPos);
                    break;
                case (slotType.recipe):
                    recipeTexts.Add(temp);
                    recipeLocations.Add(tempPos);
                    break;
            }

            ForceInvLocations();
            //MoveInv(true);
        }
    }
    
    public static void changeTo(Transform obj, string layer)
    {
        obj.gameObject.layer = LayerMask.NameToLayer(layer);
        foreach (Transform t in GetChildRecursive(obj.gameObject.transform))
            t.gameObject.layer = LayerMask.NameToLayer(layer);
    }

    static List<Transform> GetChildRecursive(Transform obj)
    {
        if (obj == null)
            return null;

        List<Transform> rv = new List<Transform>();

        for (int i = 0; i < obj.childCount; i++)
        {
            if (obj.GetChild(i) != null) {
                rv.Add(obj.GetChild(i));
                rv.AddRange(GetChildRecursive(obj.GetChild(i)));
            }
        }
        return rv;
    }
}
