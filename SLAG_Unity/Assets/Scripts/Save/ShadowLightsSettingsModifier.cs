﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowLightsSettingsModifier : MonoBehaviour
{
    Light light;
    private void Awake()
    {
        Settings.Instance.OnShadowsChanged += OnShadowsChanged;
    }

    private void OnDestroy()
    {
        Settings.Instance.OnShadowsChanged -= OnShadowsChanged;
    }

    void OnShadowsChanged(Shadows newShadows)
    {
        SetShadowSettings(newShadows);
    }

    void Start()
    {
        light = this.gameObject.GetComponent<Light>();
        SetShadowSettings(Settings.Instance.ShadowsSet);
    }

    void SetShadowSettings(Shadows level)
    {
        if (light == null)
            light = this.gameObject.GetComponent<Light>();

        switch (level)
        {
            case Shadows.Low:
                light.shadowResolution = UnityEngine.Rendering.LightShadowResolution.Low;
                break;
            case Shadows.Mid:
                light.shadowResolution = UnityEngine.Rendering.LightShadowResolution.Medium;
                break;
            case Shadows.High:
                light.shadowResolution = UnityEngine.Rendering.LightShadowResolution.VeryHigh;
                break;
            default:
                light.shadowResolution = UnityEngine.Rendering.LightShadowResolution.Medium;
                break;

        }
    }
}
