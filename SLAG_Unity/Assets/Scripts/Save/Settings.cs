﻿using UnityEngine;

public enum WorldDetail
{
    Off,
    Low,
    Mid,
    High
}
public enum Shadows
{
    Low,
    Mid,
    High
}

public class Settings
{
    private static Settings _instance;
    public static Settings Instance
    {
        get
        {
            if (_instance == null)
                _instance = new Settings();

            return _instance;
        }
    }

    public WorldDetail WorldDetailSet { get; private set; }
    public Shadows ShadowsSet { get; private set; }

    private Settings()
    {
        WorldDetailSet = WorldDetail.Mid;
        ShadowsSet = Shadows.High;
    }

    public delegate void WorldDetailChangeHandler(WorldDetail newWorldDetail);
    public event WorldDetailChangeHandler OnWorldDetailChanged;

    public void ChangeWorldDetail()
    {
		switch (WorldDetailSet)
		{
            case WorldDetail.Off:
                WorldDetailSet = WorldDetail.Low;
                break;
            case WorldDetail.Low:
                WorldDetailSet = WorldDetail.Mid;
                break;
            case WorldDetail.Mid:
                WorldDetailSet = WorldDetail.High;
                break;
            case WorldDetail.High:
                WorldDetailSet = WorldDetail.Off;
                break;
        }

        OnWorldDetailChanged?.Invoke(WorldDetailSet);
    }

    public delegate void ShadowsChangeHandler(Shadows newShadows);
    public event ShadowsChangeHandler OnShadowsChanged;

    public void ChangeShadows()
    {
        switch (ShadowsSet)
        {
            case Shadows.Low:
                ShadowsSet = Shadows.Mid;
                break;
            case Shadows.Mid:
                ShadowsSet = Shadows.High;
                break;
            case Shadows.High:
                ShadowsSet = Shadows.Low;
                break;
        }

        OnShadowsChanged?.Invoke(ShadowsSet);
    }
}