﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowSettingsModifier : MonoBehaviour
{
    public bool lowRecieve = true;
    public UnityEngine.Rendering.ShadowCastingMode lowCast = UnityEngine.Rendering.ShadowCastingMode.On;

    public bool midRecieve = true;
    public UnityEngine.Rendering.ShadowCastingMode midCast = UnityEngine.Rendering.ShadowCastingMode.On;

    public bool highRecieve = true;
    public UnityEngine.Rendering.ShadowCastingMode highCast = UnityEngine.Rendering.ShadowCastingMode.On;

    MeshRenderer MR;

    private void Awake()
    {
        Settings.Instance.OnShadowsChanged += OnShadowsChanged;
    }

	private void OnDestroy()
	{
        Settings.Instance.OnShadowsChanged -= OnShadowsChanged;
    }

	void OnShadowsChanged(Shadows newShadows)
	{
        SetShadowSettings(newShadows);
    }

    void Start()
    {
        MR = this.gameObject.GetComponent<MeshRenderer>();
        SetShadowSettings(Settings.Instance.ShadowsSet);
    }

    void SetShadowSettings(Shadows level)
	{
        if(MR == null)
            MR = this.gameObject.GetComponent<MeshRenderer>();

        switch(level){
            case Shadows.Low:
                MR.receiveShadows = lowRecieve;
                MR.shadowCastingMode = lowCast;
                break;
            case Shadows.Mid:
                MR.receiveShadows = midRecieve;
                MR.shadowCastingMode = midCast;
                break;
            case Shadows.High:
                MR.receiveShadows = highRecieve;
                MR.shadowCastingMode = highCast;
                break;
            default:
                MR.receiveShadows = midRecieve;
                MR.shadowCastingMode = midCast;
                break;
			
        }
	}
}
