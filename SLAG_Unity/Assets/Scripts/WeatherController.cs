﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EntroPi;

public class WeatherController : MonoBehaviour
{
    //affects cloud movement direction
    //affects rain angle direction TODO
    //-180 - 180
    float windDirection = 0;
    bool windDirectionDir = false;

    //affects cloud movement speed
    //affects steepness of rain angle TODO
    //0 - 5? (no cap, but for niceness)
    float windSpeed = 1.25f;
    bool windSpeedDir = false;

    //affects amount of cloud coverage
    //affects heavyness of rain (not just this) TODO
    //affects sun brightness ? TODO
    //-1 - 1
    float cloudCoverageMod = 0;
    bool cloudCoverageDir = false;

    //affects rain heavyness, with cloudCoverageMod as cap? TODO
    //EG if rainHeavyness is 0, cloudCoverageMod is 1, still no rain
    //will need something in place to allow for more sudden changes
    //0 - 1
    float rainHeavyness = 0;

    public CloudShadows clouds;

    //TODO seasons affecting likelyness of rain etc

    void Start()
    {
        
    }


    void FixedUpdate()
    {
		if (GameStateManager.Instance.CurrentGameState != GameState.Paused)
		{
            updateCloudCoverage();
            updateWindDirection();
            updateWindSpeed();
        }
		else
		{
            clouds.SpeedMultiplier = 0;
        }
    }

    void updateWindSpeed()
    {
        //change wind speed over time
        int randomOfThree = Random.Range(0, 3);
        if (randomOfThree == 0)
            windSpeed += Random.Range((windSpeedDir ? -0.005f : -0.025f), (windSpeedDir ? 0.01f : 0.005f));

        //clamp to a reasonable range
        windSpeed = Mathf.Clamp(windSpeed, 0, 5);

        //Change wind speed modifer movement average dir
        if (Random.Range(0, 750) == 0)
        {
            if(windSpeed < 0.5f)
                windSpeedDir = !windSpeedDir;
			else
			{
                int randomOfNine = Random.Range(0, 9);
                //more likely to move down / be slow
                if (randomOfNine < 5)
                    windSpeedDir = false;
                else
                    windSpeedDir = true;

            }
        }

        //set clouds speed dependant on it
        clouds.SpeedMultiplier = windSpeed;
    }

    void updateWindDirection()
	{
        //change wind direction over time
        int randomOfThree = Random.Range(0, 3);
        if (randomOfThree == 0)
            windDirection += Random.Range((windDirectionDir ? -0.5f : -0.9f), (windDirectionDir ? 0.9f : 0.5f));

        //make sure always in range -180 - 180. wraps around rather than clamped
        if (windDirection < -180)
            windDirection += 360;
        else if (windDirection > 180)
            windDirection -= 360;

        //Change wind direction modifer movement average dir
        if (Random.Range(0, 1000) == 0)
        {
            windDirectionDir = !windDirectionDir;
        }

        //set clouds direction dependant on it
        clouds.DirectionModifier = windDirection;
    }

    void updateCloudCoverage()
	{
        //Change cloud coverage over time
        //weighted to move in the diretion it was already going
        int randomOfThree = Random.Range(0, 3);
        if (randomOfThree == 0)
            cloudCoverageMod += Random.Range((cloudCoverageDir ? -0.001f : -0.002f), (cloudCoverageDir ? 0.002f : 0.001f));

        //Change cloud coverage modifer movement average dir
        if (Random.Range(0,1200) == 0)
		{
            int randomOfNine = Random.Range(0, 9);
            //more likely to move down / be clear
            //TODO have seasons affect this
            if (randomOfNine < 5)
                cloudCoverageDir = false;
            else
                cloudCoverageDir = true;
            //UnityEngine.Debug.Log("cloudCoverageDir change " + cloudCoverageDir);
		}

        cloudCoverageMod = Mathf.Clamp(cloudCoverageMod, -0.7f, 0.6f);
        clouds.CoverageModifier = cloudCoverageMod;
    }
}
