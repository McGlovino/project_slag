﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class DayController : MonoBehaviour
{
    List<Light> lights = new List<Light>();
    List<float> startBrightness = new List<float>();

    float time = 0;
    float dayLength = 4f * 60;
    float nightLength = 2f * 60;

    bool day = true;

    void Start()
    {
        time = dayLength / 2;

        foreach (Light l in GetComponentsInChildren<Light>())
        {
            lights.Add(l);
            startBrightness.Add(l.intensity);
        }
        Light cloudsLight = GameObject.FindGameObjectWithTag("clouds").GetComponent<Light>();
        lights.Add(cloudsLight);
        startBrightness.Add(cloudsLight.intensity);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity = 0; //(startBrightness[i] / 100) * 1;
            }
            time = dayLength + (nightLength / 2);
            day = false;
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity = (startBrightness[i]);
            }
            time = dayLength / 2;
            day = true;
        }

        //day
        if (time <= dayLength / 2)
        {
            if (day == false)
                day = true;

            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity += (startBrightness[i] / dayLength) * Time.deltaTime;
            }
        }
        else if (time <= dayLength)
        {
            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity -= (startBrightness[i] / dayLength) * Time.deltaTime;
            }
        }
        //night
        else if (time <= dayLength + (nightLength / 2))
        {
            if (day == true)
                day = false;

            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity -= (startBrightness[i] / nightLength) * Time.deltaTime;
            }
        }
        else if (time <= dayLength + nightLength)
        {
            for (int i = 0; i < lights.Count; i++)
            {
                lights[i].intensity += (startBrightness[i] / nightLength) * Time.deltaTime;
            }
        }

        time += Time.deltaTime;

        if (time > dayLength + nightLength)
            time = 0;
    }
}
