﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public FlockingManager myManager;
    float speed;
    bool turning = false;
    Bounds limit;

    void Start()
    {
        speed = Random.Range(myManager.minSpeed, myManager.maxSpeed);
        limit = new Bounds(myManager.transform.position, myManager.swimLimit);
    }

    void Update()
    {
        if (!limit.Contains(transform.position))
            turning = true;
        else
            turning = false;


        if (Random.Range(0, 100) < 1)
            speed = Random.Range(myManager.minSpeed, myManager.maxSpeed);

        if (turning)
        {
            Vector3 direction = myManager.transform.position - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), myManager.rotSpeed * Time.deltaTime);
        }
        else if (Random.Range(0, 100) < 40)
            ApplyRules();

        transform.Translate(new Vector3(0,0,speed*Time.deltaTime));
    }

    void ApplyRules()
    {
        GameObject[] fish;
        fish = myManager.allFish;

        Vector3 avCenter = Vector3.zero;
        Vector3 avAvoid = Vector3.zero;
        float totSpeed = 0;
        float distance;
        int groupSize = 0;

        foreach (GameObject g in fish)
        {
            if (g == gameObject)
                continue;

            distance = (transform.position - g.transform.position).magnitude;

            if (distance > myManager.neighbourDistance)
                continue;

            avCenter += g.transform.position;
            groupSize++;

            if (distance < 1f)
                avAvoid += (transform.position - g.transform.position);

            Flock otherFlock = g.GetComponent<Flock>();
            totSpeed += otherFlock.speed;
        }

        if(groupSize > 0)
        {
            avCenter = avCenter / groupSize + (myManager.goalPos - transform.position)/3;
            speed = totSpeed / groupSize;

            Vector3 heading = (avCenter + avAvoid) - transform.position;
            if (heading != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(heading), myManager.rotSpeed * Time.deltaTime);
        }
    }
}
