﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockingManager : MonoBehaviour
{
    public int weightedSpawn = 15;

    GameObject fishPrefab;
    int numFish;
    [HideInInspector]
    public GameObject[] allFish;
    [HideInInspector]
    public Vector3 swimLimit;
    //public int swimLimit = 5;
    [HideInInspector]
    public Vector3 goalPos;

    [HideInInspector]
    public float minSpeed = 0.1f;
    [HideInInspector]
    public float maxSpeed = 0.6f;
    [HideInInspector]
    public float neighbourDistance = 2f;
    [HideInInspector]
    public float rotSpeed = 1.3f;
    GameObject player;
    [HideInInspector]
    public float playerLoadDistance = 25f;
    bool hasBeenOff = false;


    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        numFish = (int)(Random.Range(0, weightedSpawn) * transform.localScale.x);
        swimLimit = new Vector3(transform.localScale.x, 0, transform.localScale.z)*7f;

        fishPrefab = (GameObject)Resources.Load("Prefabs/Fish");

        allFish = new GameObject[numFish];
        for (int i = 0; i < allFish.Length; i++)
        {
            Vector3 pos = transform.position + new Vector3(Random.Range(-swimLimit.x, swimLimit.x),
                                swimLimit.y,
                                Random.Range(-swimLimit.z, swimLimit.z));

            allFish[i] = (GameObject)Instantiate(fishPrefab, pos, Quaternion.identity, transform);
            allFish[i].GetComponent<Flock>().myManager = this;
        }
        goalPos = transform.position;
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, player.transform.position) > playerLoadDistance)
        {
            if (!hasBeenOff)
            {
                hasBeenOff = true;
                foreach (GameObject g in allFish)
                    g.SetActive(false);
            }
        }
        else
        {
            if (hasBeenOff) {
                hasBeenOff = false;
                foreach (GameObject g in allFish)
                    g.SetActive(true);
            }
                
            if (Random.Range(0, 100) < 5)
            {
                goalPos = transform.position + new Vector3(Random.Range(-swimLimit.x, swimLimit.x),
                                    swimLimit.y,
                                    Random.Range(-swimLimit.z, swimLimit.z));
            }
        }
    }
}
