﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    string previous = " ";

    void Update()
    {
        RaycastHit hit;
        Physics.Raycast(this.transform.position + new Vector3(0, 0.2f, 0), Vector3.down, out hit, 15f);

        if (hit.transform.tag != previous)
        {
            switch (hit.transform.tag)
            {
                case "Swamp":
                    //Trigger music
                    AkSoundEngine.SetSwitch("Footsteps", "Swamp", gameObject);
                    AkSoundEngine.PostEvent("Play_Swamp_BGM", gameObject);
                    previous = hit.transform.tag;
                    break;
                case "Grassy":
                    //Trigger music
                    AkSoundEngine.PostEvent("Stop_Swamp_BGM", gameObject);
                    AkSoundEngine.SetSwitch("Footsteps", "Default", gameObject);
                    previous = hit.transform.tag;
                    break;
                case "Forest":
                    //Trigger music
                    AkSoundEngine.PostEvent("Stop_Swamp_BGM", gameObject);
                    AkSoundEngine.SetSwitch("Footsteps", "Default", gameObject);
                    previous = hit.transform.tag;
                    break;
                case "Rocky":
                    //Trigger music
                    AkSoundEngine.PostEvent("Stop_Swamp_BGM", gameObject);
                    AkSoundEngine.SetSwitch("Footsteps", "Default", gameObject);
                    previous = hit.transform.tag;
                    break;
                case "Desert":
                    //Trigger music
                    AkSoundEngine.PostEvent("Stop_Swamp_BGM", gameObject);
                    AkSoundEngine.SetSwitch("Footsteps", "Default", gameObject);
                    previous = hit.transform.tag;
                    break;
                default:
                    break;
            }
        }
    }
}
