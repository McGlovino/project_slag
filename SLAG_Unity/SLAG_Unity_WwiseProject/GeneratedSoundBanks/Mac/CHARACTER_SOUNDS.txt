Event	ID	Name			Wwise Object Path	Notes
	1704217537	Play_Default_Footsteps			\Default Work Unit\CHARACTER_SOUNDS\Play_Default_Footsteps	
	2019928655	Stop_Default_Footsteps			\Default Work Unit\CHARACTER_SOUNDS\Stop_Default_Footsteps	
	2494826898	Play_Swamp_Footsteps			\Default Work Unit\CHARACTER_SOUNDS\Play_Swamp_Footsteps	
	3003882221	Play_Jump_Voice			\Default Work Unit\CHARACTER_SOUNDS\Play_Jump_Voice	

Switch Group	ID	Name			Wwise Object Path	Notes
	2385628198	Footsteps			\Default Work Unit\Footsteps\Footsteps	

Switch	ID	Name	Switch Group			Notes
	782826392	Default	Footsteps			
	2907906111	Swamp	Footsteps			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	67209736	SnowFootstep5	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\SnowFootstep5_1B31483D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Swamp_Footsteps\SnowFootstep5		130528
	215842824	Footstep4	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\Footstep4_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Default_Footsteps\Footstep4		58944
	295359148	JumpVoice3	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\JumpVoice3_222055F2.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character Sounds\Character_Jump_Voice\JumpVoice3		117436
	295384829	SnowFootstep2	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\SnowFootstep2_2E6A233C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Swamp_Footsteps\SnowFootstep2		173708
	378770573	JumpVoice1	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\JumpVoice1_FFC98C87.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character Sounds\Character_Jump_Voice\JumpVoice1		118236
	402798611	SnowFootstep3	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\SnowFootstep3_F677D78A.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Swamp_Footsteps\SnowFootstep3		173500
	560760973	Footstep2	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\Footstep2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Default_Footsteps\Footstep2		110392
	578560600	Footstep1	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\Footstep1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Default_Footsteps\Footstep1		110396
	795357302	Footstep3	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\Footstep3_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Default_Footsteps\Footstep3		95692
	815684767	SnowFootstep4	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\SnowFootstep4_ABF7B3A3.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Swamp_Footsteps\SnowFootstep4		185536
	980050616	SnowFootstep1	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\SnowFootstep1_3D1FA178.wem		\Actor-Mixer Hierarchy\Default Work Unit\Footsteps\Footsteps\Swamp_Footsteps\SnowFootstep1		148784
	1019277387	JumpVoice2	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\JumpVoice2_FFC98C87.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character Sounds\Character_Jump_Voice\JumpVoice2		118236
	1067834575	JumpVoice4	Y:\project_slag\SLAG_Unity\SLAG_Unity_WwiseProject\.cache\Mac\SFX\JumpVoice4_4F12F98F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Character Sounds\Character_Jump_Voice\JumpVoice4		121988

